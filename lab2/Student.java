public class Student {
	private String name;
	private String id;
	private String group;
	private String email;
	
	//default constructor
	public Student() {
		this.name = "Student";
		this.id = "000";
		this.group = "K59CB";
		this.email = "uet@vnu.edu.vn";
	}
	
	//parameterized constructor
	public Student(String n, String id, String em) {
		super();
		this.name = n;
		this.id = id;
		this.email = em;
		this.group = "K59CB";
	}
	
	//copy constructor
	public Student(Student s) {
		this.name = s.name;
		this.id = s.id;
		this.email = s.email;
		this.group = s.group;
	}
	
	//name getter
	public String getName() {
		return name;
	}
	
	//name setter
	public void setName(String n) {
		this.name = n;
	}
	
	//id getter
	public String getId() {
		return id;
	}
	
	//id setter
	public void setId(String Id) {
		this.id = Id;
	}
	
	//group getter
	public String getGroup() {
		return group;
	}
	
	//group setter
	public void setGroup(String Gr) {
		this.group = Gr;
	}
	
	//email getter
	public String getEmail() {
		return email;
	}
	
	//email setter
	public void setEmail(String e) {
		this.email = e;
	}
	
	//get all info
	public String getInfo() {
		return ("Name: " + name + "\tID: " + id + "\tGroup: " + group + "\tEmail: " + email);
	}
}
