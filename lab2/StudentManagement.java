public class StudentManagement {
	
	//check if two students are in same group
	public boolean isSameGroup(Student s1, Student s2) {
		if(s1.getGroup().equals(s2.getGroup())) return true;
		else return false;
	}
	
	public static void main(String[] args) {
		//create a new student
		Student a = new Student();
		
		//set its attributes
		a.setName("Nguyen Duy Hieu");
		a.setId("13020155");
		a.setGroup("K58CC");
		a.setEmail("hieund_58@vnu.edu.vn");
		
		//print student's name
		System.out.println(a.getName());
		
		//print student info
		System.out.println(a.getInfo());
		
		//test parameterized constructor
		Student b = new Student("Nguyen Van B","12345678","vanb@vnu.edi.vn");
		System.out.println(b.getInfo());
		
		//test default constructor
		Student c = new Student();
		System.out.println(c.getInfo());
		
		//test copy constructor
		Student d = new Student(c);
		System.out.println(d.getInfo());
		
		//create 3 students, 2 in group K59CLC and 1 in K59CB, test the isSameGroup method
		Student s1 = new Student("A","","");
		s1.setGroup("K59CLC");
		Student s2 = new Student("B","","");
		s2.setGroup("K59CLC");
		Student s3 = new Student("C","","");
		
		StudentManagement sm = new StudentManagement();
		
		if(sm.isSameGroup(s1,s2)) System.out.println(s1.getName() + " cung lop " + s2.getName());
		else System.out.println(s1.getName() + " khac lop " + s2.getName());
		
		if(sm.isSameGroup(s1,s3)) System.out.println(s1.getName() + " cung lop " + s3.getName());
		else System.out.println(s1.getName() + " khac lop " + s3.getName());
		
	}
}
