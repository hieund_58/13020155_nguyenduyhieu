import java.util.ArrayList;

public class Diagram {
	private ArrayList<Layer> layers;
	
	public Diagram(){
		layers = null;
	}

	public Diagram(ArrayList<Layer> layers) {
		super();
		this.layers = layers;
	}

	public ArrayList<Layer> getLayers() {
		return layers;
	}

	public void setLayers(ArrayList<Layer> layers) {
		this.layers = layers;
	}
	
	public int countLayers(){
		return layers == null ? 0 : layers.size();
	}
	
	public void deleteCircle() {
		if(countLayers() > 0) {
			for(Layer l : getLayers()) {
				l.deleteCircle();
			}
		}
	}

	@Override
	public String toString() {
		//return "Diagram has " + countLayers() + " layers:\n\t" + layers;
		StringBuffer temp = new StringBuffer();
		if(countLayers() > 0) {
			temp.append("Diagram has " + countLayers() + " layers:\n");
			for(int i = 0; i < countLayers(); i++){
				temp.append("\tLayer " + i + " has shapes:\n" + layers.get(i).toString());
			}
			return temp.toString();
			}
		else
			return "Diagram has no layer";
	}	
}
