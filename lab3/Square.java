
public class Square extends Rectangle {
	public Square(){
		super();
		//double side = 1;
	}
	
	public Square(double side){
		super(side,side);
		//side = side;
	}
	
	public Square(double side, String color, boolean filled){
		super(side, side, color, filled);
		//side = side;
	}
	
	public double getSide(){
		return super.getLength();
	}
	
	public void setSide(double side){
		super.setLength(side);
		super.setWidth(side);
	}
	
	@Override
	public void setWidth(double side){
		super.setWidth(side);
		super.setLength(side);
	}
	
	@Override
	public void setLength(double side){
		super.setLength(side);
		super.setWidth(side);
	}

	@Override
	public String toString() {
		return "\t\tSquare [side=" + getSide() + ", color=" + getColor() + ", isFilled=" + isFilled() + "]\n";
	}
}
