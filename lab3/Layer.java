import java.util.ArrayList;

public class Layer {
	private ArrayList<Shape> shapes;
	
	public Layer(){
		shapes = null;
	}

	public Layer(ArrayList<Shape> shapes) {
		super();
		this.shapes = shapes;
	}

	public ArrayList<Shape> getShapes() {
		return shapes;
	}

	public void setShapes(ArrayList<Shape> shapes) {
		this.shapes = shapes;
	}
	
	public int countShapes(){
		return shapes == null ? 0 : shapes.size();
	}
	
	public void deleteCircle(){
		if(countShapes() > 0) {
			for(int i = countShapes()-1; i >= 0; i--){
				if(shapes.get(i) instanceof Circle) {
					shapes.remove(i);
				}
			}
		}
	}
	
	public void deleteTriangle(){
		if(countShapes() > 0) {
			for(int i = countShapes()-1; i >= 0; i--){
				if(shapes.get(i) instanceof Triangle) {
					shapes.remove(i);
				}
			}
		}
	}

	@Override
	public String toString() {
		//return "Layer has shapes: \n \t" + shapes + "\n";
		//return shapes + " ";
		StringBuffer temp = new StringBuffer();
		if(shapes != null){
			for(int i = 0; i < countShapes(); i++){
				temp.append(shapes.get(i).toString());
			}
		}
		return temp.toString();
	}	
	
}
