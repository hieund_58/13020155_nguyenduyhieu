import java.util.ArrayList;

public class Main {
	public static void main(String[] args){
		
		//create new shapes
		Circle s1c1 = new Circle();
			s1c1.setColor("Green");
			s1c1.setRadius(2);
			s1c1.getArea();
		Rectangle s1r1 = new Rectangle();
		Square s2s1 = new Square(6);
		Circle s2c1 = new Circle(7, "purple", true);
		
		//create array list shape to store shapes created above
		ArrayList<Shape> s1 = new ArrayList<Shape>(5);
		ArrayList<Shape> s2 = new ArrayList<Shape>(6);
		s1.add(s1c1);
		s1.add(s1r1);
		s1.add(new Triangle(1,2,3));
		s1.add(new Triangle(2,2,2));
		s2.add(s2s1);
		s2.add(s2c1);
		s2.add(new Triangle(4,5,6));
		s2.add(new Square());
		
		//create layer to pass the shapes
		Layer l1 = new Layer();
		l1.setShapes(s1);
		Layer l2 = new Layer();
		l2.setShapes(s2);
		
		//create array list layer to store layers created above
		ArrayList<Layer> l = new ArrayList<Layer>();
		l.add(l1);
		l.add(l2);
		
		//create diagram to fetch layer
		Diagram test = new Diagram();
		test.setLayers(l);
		
		//test toString method
		System.out.println(test);			
		//test color setter
		test.getLayers().get(0).getShapes().get(0).setColor("blue");		
		//test deleteTriangle method from layer class, delete all triangle from layer 1
		test.getLayers().get(1).deleteTriangle();
		//test deleteCircle method from diagram class, delete all circle from diagram
		test.deleteCircle();
		System.out.println(test);			
		
//		Circle c = (Circle) test.getLayers().get(0).getShapes().get(0);
//		c.getArea();
//		for(Layer layer : test.getLayers()) {
//		layer.deleteCircle();
//	}
	}
}
