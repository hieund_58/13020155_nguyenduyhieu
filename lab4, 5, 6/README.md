## Compile Main.java, see the result.
1. lab 4, 5: 
	- method deleteDuplicateShapes() in Layer.java to delete duplicate shapes. Shapes are duplicate if they are equal. Every shape object has overriden equal method.
	- method makeOneShapeTypedLayers() in Diagram.java to move same shape-type objects into each layer. 
	- 2 methods don't need to changed even if we add more class extends shape.
2. lab 6:
	- 2 methods to serialize and deserialize Diagram object are in Diagram.java