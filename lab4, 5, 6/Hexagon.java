import java.io.Serializable;

public class Hexagon extends Shape implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6562333736929650525L;
	private Point upperLeftVertex, lowerLeftVertex;
	
	public Hexagon() {
		super();
		upperLeftVertex = new Point(0,0);
		lowerLeftVertex = new Point(0,-1);
	}

	public Hexagon(Point upperLeftVertex, Point lowerLeftVertex, String color, boolean filled) {
		super(color, filled);
		// TODO Auto-generated constructor stub
		this.upperLeftVertex = upperLeftVertex;
		this.lowerLeftVertex = lowerLeftVertex;
	}

	public Point getUpperLeftVertex() {
		return upperLeftVertex;
	}

	public void setUpperLeftVertex(Point upperLeftVertex) {
		this.upperLeftVertex = upperLeftVertex;
	}

	public Point getLowerLeftVertex() {
		return lowerLeftVertex;
	}

	public void setLowerLeftVertex(Point lowerLeftVertex) {
		this.lowerLeftVertex = lowerLeftVertex;
	}
	
	public double getSide() {
		return upperLeftVertex.distanceTo(lowerLeftVertex);
	}
	
	public double getPerimeter() {
		return getSide() * 6;
	}
	
	public double getArea() {
		return 3 * Math.sqrt(3) * Math.pow(getSide(), 2) / 2;
	}
	
	public boolean isValidated() {
		return !upperLeftVertex.equals(lowerLeftVertex);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((lowerLeftVertex == null) ? 0 : lowerLeftVertex.hashCode());
		result = prime * result + ((upperLeftVertex == null) ? 0 : upperLeftVertex.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hexagon other = (Hexagon) obj;
		if (lowerLeftVertex == null) {
			if (other.lowerLeftVertex != null)
				return false;
		} else if (!lowerLeftVertex.equals(other.lowerLeftVertex))
			return false;
		if (upperLeftVertex == null) {
			if (other.upperLeftVertex != null)
				return false;
		} else if (!upperLeftVertex.equals(other.upperLeftVertex))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "\t\tHexagon [upperLeftVertex=" + upperLeftVertex.toString() + ", lowerLeftVertex=" + lowerLeftVertex.toString() + ", color="
				+ getColor() + ", isFilled=" + isFilled() + "]\n";
	}
	
}
