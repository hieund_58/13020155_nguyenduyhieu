import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Diagram implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4152673038432230513L;
	private ArrayList<Layer> layers;
	
	public Diagram(){
		layers = null;
	}

	public Diagram(ArrayList<Layer> layers) {
		super();
		this.layers = layers;
	}

	public ArrayList<Layer> getLayers() {
		return layers;
	}

	public void setLayers(ArrayList<Layer> layers) {
		this.layers = layers;
	}
	
	public int countLayers() {
		return layers == null ? 0 : layers.size();
	}
	
	public void addLayer(Layer layer) {
		this.layers.add(layer);
	}
	
	//lab 3
	public void deleteCircle() {
		if (countLayers() > 0) {
			for (Layer l : getLayers()) {
				l.deleteCircle();
			}
		}
	}
	
	//lab 4, 5
	public void makeOneShapeTypedLayers() {
		HashMap<String, Layer> hm = new HashMap<>();
		for (Layer l : getLayers()) {
			for (Shape s : l.getShapes()) {
				hm.put(s.getClass().getSimpleName(), new Layer(true, new ArrayList<Shape>()));				
			}
		}
		for (Layer l : getLayers()) {
			for (Shape s : l.getShapes()) {
				hm.get(s.getClass().getSimpleName()).addShape(s);			
			}
		}
		this.layers = null;
		this.layers = new ArrayList<Layer>(hm.values());
	}
	
	//lab 6. Each time an Diagram object call this method, it will be serialized in to "diagram.dat"
	public void writeObjectToFile() {
//		ObjectOutputStream oos = null;
//		try {
//			FileOutputStream fos = new FileOutputStream("diagram.dat");
//			oos = new ObjectOutputStream(fos);
//			oos.writeObject(this);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
		try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("diagram.dat")))) {
			oos.writeObject(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//lab 6. Retrieve Diagram object from "diagram.dat", print it to screen
	public void readObjectFromFile() {
		ObjectInputStream ois = null;
		try {
			FileInputStream fis = new FileInputStream("diagram.dat");
			ois = new ObjectInputStream(fis);
			Diagram d = (Diagram) ois.readObject();
			System.out.println(d);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				ois.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public String toString() {
		StringBuffer temp = new StringBuffer();
		if (countLayers() > 0) {
			temp.append("Diagram has " + countLayers() + " layers:\n");
			for (int i = 0; i < countLayers(); i++) {
				int j = i + 1;
				if (layers.get(i).isVisible()) {
					temp.append("\tLayer " + j + " has shapes:\n" + layers.get(i).toString());
				}
				else {
					temp.append("\tLayer " + j + " is invisible\n");
				}
			}
			return temp.toString();
		}
		else
			return "Diagram has no layer";
	}	
}
