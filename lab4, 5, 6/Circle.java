import java.io.Serializable;

public class Circle extends Shape implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -491602914821668621L;
	private Point centre;
	private double radius;
	
	public Circle() {
		super();
		centre = new Point(0,0);
		radius = 1;
	}
	
	public Circle(Point centre, double radius) {
		super();
		this.centre = centre;
		this.radius = radius;
	}
	
	public Circle( Point centre, double radius, String color, boolean filled) {
		super(color,filled);
		this.centre = centre;
		this.radius = radius;
	}

	public Point getCentre() {
		return centre;
	}

	public void setCentre(Point centre) {
		this.centre = centre;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getArea() {
		return radius * radius * Math.PI;
	}
	
	public double getPerimeter() {
		return radius * 2 * Math.PI;
	}
	
	public boolean isValidated() {
		return radius > 0;
	}

	@Override
	public String toString() {
		if (!isValidated())
			return "Circle is not validated\n";
		else
			return "\t\tCircle [centre=" + centre.toString() + ", radius=" + radius + ", color=" + getColor() + ", isFilled=" + isFilled() + "]\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((centre == null) ? 0 : centre.hashCode());
		long temp;
		temp = Double.doubleToLongBits(radius);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Circle other = (Circle) obj;
		if (centre == null) {
			if (other.centre != null)
				return false;
		} else if (!centre.equals(other.centre))
			return false;
		if (Double.doubleToLongBits(radius) != Double.doubleToLongBits(other.radius))
			return false;
		return true;
	}
}
