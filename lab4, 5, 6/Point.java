import java.io.Serializable;

public class Point implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9094735644396088680L;
	double x,y;
	
	public Point(){
		x = y = 0;
	}

	public Point(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public double distanceTo(Point another){
		return Math.sqrt( Math.pow(this.x - another.x, 2) + Math.pow(this.y - another.y, 2) );
	}
	
	public boolean isCreatingSquare(Point p1, Point p2) {
		double a1 = this.x - p1.x;
		double a2 = this.y - p1.y;
		double b1 = this.x - p2.x;
		double b2 = this.y - p2.y;
		return a1 * b1 + a2 * b2 == 0;
	}
	
	public boolean isDuplicate(Point another){
		return this.x == another.x && this.y == another.y;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	@Override
	public String toString(){
		return "("+ x + "," + y + ")";
	}
}
