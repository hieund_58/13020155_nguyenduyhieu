import java.io.Serializable;
import java.util.ArrayList;

public class Layer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4328508599681801690L;
	private boolean visible;
	private ArrayList<Shape> shapes;
	
	public Layer(){
		visible = true;
		shapes = null;
	}

	public Layer(boolean visible, ArrayList<Shape> shapes) {
		super();
		this.visible = visible;
		this.shapes = shapes;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public ArrayList<Shape> getShapes() {
		return shapes;
	}

	public void setShapes(ArrayList<Shape> shapes) {
		this.shapes = shapes;
	}
	
	public void addShape(Shape shape) {
		this.shapes.add(shape);
	}
	
	public int countShapes() {
		return shapes == null ? 0 : shapes.size();
	}
	
	//lab 3
	public void deleteCircle() {
		if (countShapes() > 0) {
			for (int i = countShapes()-1; i >= 0; i--) {
				if (shapes.get(i) instanceof Circle) {
					shapes.remove(i);
				}
			}
		}
	}
	
	//lab 3
	public void deleteTriangle() {
		if (countShapes() > 0) {
			for (int i = countShapes()-1; i >= 0; i--) {
				if (shapes.get(i) instanceof Triangle) {
					shapes.remove(i);
				}
			}
		}
	}
	
	//lab 4, 5
	public void deleteDuplicateShapes() {
		if (countShapes() > 1) {
			for (int i = countShapes() - 1; i > 0; i--) {
				for (int j = i-1; j >= 0; j--) {
					if (shapes.get(i).equals(shapes.get(j))) {
						shapes.remove(j);
						i--;
					}
				}
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuffer temp = new StringBuffer();
		if (shapes != null) {
			for (int i = 0; i < countShapes(); i++) {
				temp.append(shapes.get(i).toString());
			}
		}
		return temp.toString();
	}	
}
