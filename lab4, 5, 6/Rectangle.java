import java.io.Serializable;

public class Rectangle extends Shape implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -922327588363690833L;
	private Point upperLeftVertex, lowerLeftVertex, upperRightVertex;
	
	public Rectangle() {
		super();
		upperLeftVertex = new Point(0,0);
		lowerLeftVertex = new Point(0,-1);
		upperRightVertex = new Point(1,0);
	}	
	
	public Rectangle(Point upperLeftVertex, Point lowerLeftVertex, Point upperRightVertex) {
		super();
		this.upperLeftVertex = upperLeftVertex;
		this.lowerLeftVertex = lowerLeftVertex;
		this.upperRightVertex = upperRightVertex;
	}
	
	public Rectangle(Point upperLeftVertex, Point lowerLeftVertex, Point upperRightVertex, String color, boolean filled) {
		super(color, filled);
		// TODO Auto-generated constructor stub
		this.upperLeftVertex = upperLeftVertex;
		this.lowerLeftVertex = lowerLeftVertex;
		this.upperRightVertex = upperRightVertex;
	}
	
	//Constructor for subclass Square which needs only 2 points
	public Rectangle(Point upperLeftVertex, Point lowerLeftVertex, String color, boolean filled) {
		super(color, filled);
		// TODO Auto-generated constructor stub
		this.upperLeftVertex = upperLeftVertex;
		this.lowerLeftVertex = lowerLeftVertex;
		this.upperRightVertex = null;
	}

	public Point getUpperLeftVertex() {
		return upperLeftVertex;
	}

	public void setUpperLeftVertex(Point upperLeftPoint) {
		this.upperLeftVertex = upperLeftPoint;
	}

	public Point getLowerLeftVertex() {
		return lowerLeftVertex;
	}

	public void setLowerLeftVertex(Point lowerLeftVertex) {
		this.lowerLeftVertex = lowerLeftVertex;
	}

	public double getWidth() {
		if(upperLeftVertex.distanceTo(lowerLeftVertex) <= upperLeftVertex.distanceTo(upperRightVertex))
			return upperLeftVertex.distanceTo(lowerLeftVertex);
		else
			return upperLeftVertex.distanceTo(upperRightVertex);
	}

	public double getLength() {
		if (upperLeftVertex.distanceTo(lowerLeftVertex) >= upperLeftVertex.distanceTo(upperRightVertex))
			return upperLeftVertex.distanceTo(lowerLeftVertex);
		else
			return upperLeftVertex.distanceTo(upperRightVertex);
	}
	
	public double getArea(){
		return getWidth() * getLength();
	}
	
	public double getPerimeter() {
		return 2 * (getWidth() + getLength());
	}
	
	public boolean isValidated() {
		return upperLeftVertex.distanceTo(lowerLeftVertex) != 0 && lowerLeftVertex.distanceTo(upperRightVertex) != 0 && upperRightVertex.distanceTo(upperLeftVertex) != 0 && upperLeftVertex.isCreatingSquare(lowerLeftVertex, upperRightVertex);
	}

	@Override
	public String toString() {
		if (!isValidated())
			return "\t\tRectangle is not validated\n";
		else
			return "\t\tRectangle [upperLeftVertex=" + upperLeftVertex.toString() + ", lowerLeftVertex=" + lowerLeftVertex.toString() + ", upperRightVertex=" + upperRightVertex.toString() + ", color=" + getColor() + ", isFilled="
					+ isFilled() + "]\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((lowerLeftVertex == null) ? 0 : lowerLeftVertex.hashCode());
		result = prime * result + ((upperLeftVertex == null) ? 0 : upperLeftVertex.hashCode());
		result = prime * result + ((upperRightVertex == null) ? 0 : upperRightVertex.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rectangle other = (Rectangle) obj;
		if (lowerLeftVertex == null) {
			if (other.lowerLeftVertex != null)
				return false;
		} else if (!lowerLeftVertex.equals(other.lowerLeftVertex))
			return false;
		if (upperLeftVertex == null) {
			if (other.upperLeftVertex != null)
				return false;
		} else if (!upperLeftVertex.equals(other.upperLeftVertex))
			return false;
		if (upperRightVertex == null) {
			if (other.upperRightVertex != null)
				return false;
		} else if (!upperRightVertex.equals(other.upperRightVertex))
			return false;
		return true;
	}
	
}
