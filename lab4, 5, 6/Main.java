import java.util.ArrayList;

public class Main {
	public static void main(String[] args){		
		Layer l1 = new Layer(true, new ArrayList<Shape>());
		Layer l2 = new Layer(true, new ArrayList<Shape>());
		
		l1.addShape(new Triangle(new Point(1,2), new Point(3,4), new Point(5,10)));
		l1.addShape(new Triangle(new Point(3,4), new Point(1,2), new Point(5,10)));
		l1.addShape(new Triangle(new Point(5,10), new Point(3,4), new Point(1,2)));
		
		Circle[] c = new Circle[5];
		c[0] = new Circle();
		c[1] = new Circle(new Point(2,3), 4);
		c[2] = new Circle(new Point(1,2), -3, "purple", true);
		c[3] = new Circle();	
		c[4] = new Circle(new Point(2,3), 4);
		for(Circle temp : c) {
			if (temp.isValidated()){
				l1.addShape(temp);
			}
		}
		
		Triangle[] t = new Triangle[5];
		for (int i = 0; i < 5; i++) {
			t[i] = new Triangle();
		}
		for (Triangle temp: t) {
			if(temp.isValidated()) {
				l1.addShape(temp);
			}			
		}
		
		Rectangle[] r = new Rectangle[3];
		for (int i = 0; i < 3; i++) {
			r[i] = new Rectangle();
			if(r[i].isValidated()) {
				l2.addShape(r[i]);
			}
		}
		
		Square[] s = new Square[4];
		for(int i = 0; i < 3; i++) {
			s[i] = new Square();
			if(s[i].isValidated()) {
				l2.addShape(s[i]);
			}
		}
		
		Hexagon[] h = new Hexagon[4];
		for(int i = 0; i < 3; i++) {
			h[i] = new Hexagon();
			if(h[i].isValidated()) {
				l2.addShape(h[i]);
			}
		}
		
		Diagram test = new Diagram(new ArrayList<Layer>());
		test.addLayer(l1);
		test.addLayer(l2);
		System.out.println(test);
		
		//test setVisible, lab 4
		System.out.println("After setVisible(false) to layer 1");
		test.getLayers().get(0).setVisible(false);	
		System.out.println(test);
		
		//test delete duplicate shapes, lab 4 5
		System.out.println("After delete duplicate shapes");
		test.getLayers().get(0).setVisible(true);	
		test.getLayers().get(0).deleteDuplicateShapes();
		test.getLayers().get(1).deleteDuplicateShapes();
		System.out.println(test);
		
		//test move each type of shape to each layer, lab 4 5
		System.out.println("After move each type of shape to each layer");
		test.makeOneShapeTypedLayers();
		System.out.println(test);
		
		//test serialization, lab 6
		System.out.println("Serialize this Diagram object and then deserialize it, print it to screen");
		test.writeObjectToFile();
		test.readObjectFromFile();
	}
}
