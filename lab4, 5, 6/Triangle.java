import java.io.Serializable;

public class Triangle extends Shape implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3498096650386805876L;
	private Point vertex1, vertex2, vertex3;
	
	public Triangle() {
		super();
		vertex1 = new Point(0,0);
		vertex2 = new Point(0,1);
		vertex3 = new Point(1,1);
	}		

	public Triangle(Point vertex1, Point vertex2, Point vertex3) {
		super();
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.vertex3 = vertex3;
	}

	public Triangle(Point vertex1, Point vertex2, Point vertex3, String color, boolean filled) {
		super(color, filled);
		// TODO Auto-generated constructor stub
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.vertex3 = vertex3;
	}
	
	public Point getVertex1() {
		return vertex1;
	}

	public void setVertex1(Point vertex1) {
		this.vertex1 = vertex1;
	}

	public Point getVertex2() {
		return vertex2;
	}

	public void setVertex2(Point vertex2) {
		this.vertex2 = vertex2;
	}

	public Point getVertex3() {
		return vertex3;
	}

	public void setVertex3(Point vertex3) {
		this.vertex3 = vertex3;
	}
	
	public double getSide1() {
		return vertex1.distanceTo(vertex2);
	}
	
	public double getSide2() {
		return vertex2.distanceTo(vertex3);
	}
	
	public double getSide3() {
		return vertex3.distanceTo(vertex1);
	}

	public double getPerimeter() {
		return vertex1.distanceTo(vertex2) + vertex2.distanceTo(vertex3) + vertex3.distanceTo(vertex1);
	}
	
	public double getArea() {
		double p = getPerimeter() / 2;
		return Math.sqrt(p * (p - getSide1()) * (p - getSide2()) * (p - getSide3()));
	}
	
	public boolean isValidated() {
		return ((getSide1() + getSide2()) > getSide3()) && ((getSide1() + getSide3()) > getSide2()) && ((getSide2() + getSide3()) > getSide1());
	}
	
	@Override
	public String toString() {
		if (!isValidated()) 
			return "\t\tTriangle is not valid\n";
		else
			return "\t\tTriangle [vertex1=" + vertex1.toString() + ", vertex2=" + vertex2.toString() + ", vertex3=" + vertex3.toString() + ", color=" + getColor()
					+ ", isFilled=" + isFilled() + "]\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((vertex1 == null) ? 0 : vertex1.hashCode());
		result = prime * result + ((vertex2 == null) ? 0 : vertex2.hashCode());
		result = prime * result + ((vertex3 == null) ? 0 : vertex3.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Triangle other = (Triangle) obj;
//		if (vertex1 == null) {
//			if (other.vertex1 != null)
//				return false;
//		} else if (!vertex1.equals(other.vertex1))
//			return false;
//		if (vertex2 == null) {
//			if (other.vertex2 != null)
//				return false;
//		} else if (!vertex2.equals(other.vertex2))
//			return false;
//		if (vertex3 == null) {
//			if (other.vertex3 != null)
//				return false;
//		} else if (!vertex3.equals(other.vertex3))
//			return false;
		if (vertex1 == null || vertex2 == null || vertex3 == null || other.vertex1 == null || other.vertex2 == null || other.vertex3 == null){
			return false;
		}
		else {
			if (vertex1.equals(other.vertex1) || vertex1.equals(other.vertex2) || vertex1.equals(other.vertex3)) {
				if (vertex2.equals(other.vertex1) || vertex2.equals(other.vertex2) || vertex2.equals(other.vertex3)) {
					if (vertex3.equals(other.vertex1) || vertex3.equals(other.vertex2) || vertex3.equals(other.vertex3)) {
						return true;
					}
				}			
			}
		}
		return false;
	}	
	
}
