import java.io.Serializable;

public class Square extends Rectangle implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8843526426561629071L;

	public Square() {
		super();
	}
	
	public Square(Point upperLeftVertex, Point lowerLeftVertex, String color, boolean filled) {
		super(upperLeftVertex, lowerLeftVertex, color, filled);
	}
	
	public Point getUpperLeftVertex() {
		return super.getUpperLeftVertex();
	}
	
	public void setUpperLeftVertex(Point upperLeftVertex) {
		super.setUpperLeftVertex(upperLeftVertex);
	}
	
	public Point getLowerLeftVertex() {
		return super.getLowerLeftVertex();
	}
	
	public void setLowerLeftVertex(Point lowerLeftVertex) {
		super.setLowerLeftVertex(lowerLeftVertex);
	}
	
	public double getSide() {
		return getUpperLeftVertex().distanceTo(getLowerLeftVertex());
	}
	
	@Override
	public double getArea(){
		return getSide() * getSide();
	}
	
	@Override
	public double getPerimeter() {
		return 4 * getSide();
	}
	
	@Override
	public boolean isValidated() {
		return getUpperLeftVertex().distanceTo(getLowerLeftVertex()) != 0;
	}

	@Override
	public String toString() {
		return "\t\tSquare [upperLeftVertex=" + getUpperLeftVertex().toString() + ", lowerLeftVertex=" + getLowerLeftVertex().toString() + ", side=" + getSide() + ", color=" + getColor() + ", isFilled=" + isFilled() + "]\n";
	}
}
