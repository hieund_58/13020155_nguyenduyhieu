import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;

/**
 * @author KiLL
 *
 */
public class EndGameFrame {

	private JFrame frmGameOver;
	private User user;
	private boolean isFailed = false;

	/**
	 * Create the application.
	 */
	/**
	 * @wbp.parser.constructor
	 */
	public EndGameFrame() {
		initialize();		
	}
	
	/**
	 * Create the application
	 * @param currentUser the current user viewing this frame
	 * @param isFailed to know if that user win or fail the game
	 */
	public EndGameFrame(User currentUser, boolean isFailed) {
		this.user = new User(currentUser);
		this.isFailed = isFailed;
		initialize();		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGameOver = new JFrame();
		frmGameOver.setTitle("Game Over\r\n");
		frmGameOver.setBounds(100, 100, 433, 414);
		frmGameOver.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGameOver.setLocationRelativeTo(null);
		frmGameOver.getContentPane().setLayout(null);
		
		if (isFailed) {
			JLabel failLabel = new JLabel("<html>Sorry " + user.getName() + ", you fail the game" + "<br>" + "Your final score: " + user.getScore() + "</html>");
			failLabel.setForeground(new Color(0, 153, 255, 100));
			failLabel.setFont(new Font("Century Schoolbook", Font.BOLD, 20));
			failLabel.setHorizontalAlignment(SwingConstants.CENTER);
			failLabel.setBounds(-2, 68, 422, 120);
			frmGameOver.getContentPane().add(failLabel);
		}
		else {
			JLabel congratsLabel = new JLabel("<html>Congrats " + user.getName() + ", you pass all the questions!" + "<br>" + "You finish the game with score: " + user.getScore() + "</html>");
			congratsLabel.setForeground(new Color(0, 153, 255, 100));
			congratsLabel.setFont(new Font("Century Schoolbook", Font.BOLD, 20));
			congratsLabel.setHorizontalAlignment(SwingConstants.CENTER);
			congratsLabel.setBounds(-2, 50, 422, 120);
			frmGameOver.getContentPane().add(congratsLabel);
		}
		
		JButton contButton = new JButton("Quit to main menu");
		contButton.setFont(new Font("Meiryo", Font.BOLD, 14));
		contButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmGameOver.dispose();
				OpeningFrame op = new OpeningFrame(user);
				op.start();
			}
		});
		contButton.setBounds(114, 242, 189, 56);
		frmGameOver.getContentPane().add(contButton);
		
//		JLabel failIconLabel = new JLabel("");
//		failIconLabel.setIcon(new ImageIcon("C:\\Users\\KiLL\\Desktop\\space cool backgrounds wallpapers  Desktop Backgrounds for Free HD ....jpg"));
//		failIconLabel.setBounds(0, 6, 470, 376);
//		frmGameOver.getContentPane().add(failIconLabel);
	}
	
	/**
	 * Set this frame visible
	 */
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmGameOver.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
