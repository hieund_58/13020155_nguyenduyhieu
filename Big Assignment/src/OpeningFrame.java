import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author KiLL
 *
 */
public class OpeningFrame {

	private JFrame frmJustAQuiz;
	private JButton newGameButton, highScoresButton, changeUserButton;
	private JButton viewDBButton, addQuestionButton;
	private User currentUser;

	/**
	 * Create the application.
	 */
	/**
	 * @wbp.parser.constructor
	 */
	public OpeningFrame() {
		initialize();
	}
	
	/**
	 * @param currentUser the current user who views this frame
	 */
	public OpeningFrame(User currentUser) {
		this.currentUser = new User(currentUser);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmJustAQuiz = new JFrame();
		frmJustAQuiz.setTitle("Just a quiz game\r\n");
		frmJustAQuiz.setBounds(100, 100, 712, 540);
		frmJustAQuiz.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmJustAQuiz.setLocationRelativeTo(null);
		frmJustAQuiz.getContentPane().setLayout(null);
		
		JLabel gameLabel = new JLabel("Hello, " + currentUser.getName());
		gameLabel.setForeground(new Color(0, 0, 51, 200));
		gameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		gameLabel.setBounds(0, 24, 696, 44);
		gameLabel.setToolTipText("fdgfgfdg");
		gameLabel.setFont(new Font("Gabriola", Font.BOLD, 46));
		gameLabel.setBackground(Color.ORANGE);
		frmJustAQuiz.getContentPane().add(gameLabel);
		
		JLabel newPlayerLabel = new JLabel("This account's type is " + currentUser.getType());
		newPlayerLabel.setForeground(new Color(0, 0, 153));
		newPlayerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		newPlayerLabel.setFont(new Font("Papyrus", Font.BOLD | Font.ITALIC, 31));
		newPlayerLabel.setBounds(10, 79, 694, 43);
		frmJustAQuiz.getContentPane().add(newPlayerLabel);
		
		newGameButton = new JButton("New game");
		newGameButton.setForeground(new Color(102, 51, 102));
		newGameButton.setBounds(263, 147, 175, 55);
		newGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newgameActionPerformed(e);
			}
		});
		newGameButton.setFont(new Font("Cambria", Font.BOLD, 22));
		frmJustAQuiz.getContentPane().add(newGameButton);
		
		changeUserButton = new JButton("Change User");
		changeUserButton.setForeground(new Color(102, 51, 102));
		changeUserButton.setBounds(263, 380, 175, 55);
		changeUserButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmJustAQuiz.dispose();
				LoginFrame loginFrame = new LoginFrame();
				loginFrame.start();
			}
		});
		changeUserButton.setFont(new Font("Cambria", Font.BOLD, 22));
		frmJustAQuiz.getContentPane().add(changeUserButton);
		
		highScoresButton = new JButton("High Scores");
		highScoresButton.setForeground(new Color(102, 51, 102));
		highScoresButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hiScrActionPerformed(e);
			}
		});
		highScoresButton.setFont(new Font("Cambria", Font.BOLD, 22));
		highScoresButton.setBounds(263, 263, 175, 55);
		frmJustAQuiz.getContentPane().add(highScoresButton);
		
		if (currentUser.getType().equalsIgnoreCase("admin")) {	
			addQuestionButton = new JButton("Add Question");
			addQuestionButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					addQuestionButtActionPerformed(e);
				}
			});
			addQuestionButton.setForeground(new Color(102, 51, 102));
			addQuestionButton.setFont(new Font("Cambria", Font.BOLD, 22));
			addQuestionButton.setBounds(48, 263, 175, 55);
			frmJustAQuiz.getContentPane().add(addQuestionButton);
			
			viewDBButton = new JButton("Database");
			viewDBButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					viewDBButtActionPerformed(e);
				}
			});
			viewDBButton.setForeground(new Color(102, 51, 102));
			viewDBButton.setFont(new Font("Cambria", Font.BOLD, 22));
			viewDBButton.setBounds(483, 263, 175, 55);
			frmJustAQuiz.getContentPane().add(viewDBButton);		
		}
		
//		JLabel backgroundLabel = new JLabel("");
//		backgroundLabel.setForeground(new Color(255, 0, 153));
//		backgroundLabel.setFont(new Font("Tahoma", Font.PLAIN, 29));
//		backgroundLabel.setIcon(new ImageIcon("C:\\Users\\KiLL\\Desktop\\horsehead-nebula-dark-nebula-constellation-orion-87646.jpeg"));
//		//backgroundLabel.setIcon(new ImageIcon(OpeningFrame.class.getResource("\\a.jpg")));
//		backgroundLabel.setBounds(-109, -362, 913, 1030);
//		frmJustAQuiz.getContentPane().add(backgroundLabel);
	}
	
	/**
	 * Action when click New Game button.
	 */
	private void newgameActionPerformed(ActionEvent e) {
		frmJustAQuiz.dispose();
		InGameFrame questionFrame = new InGameFrame(currentUser);
		questionFrame.start();
	}
	
	/**
	 * Action when click High Score button.
	 */
	private void hiScrActionPerformed(ActionEvent e) {
		frmJustAQuiz.dispose();
		HighScoreFrame hs = new HighScoreFrame(currentUser, getHiScrUsersFromDB());
		hs.start();
	}
	
	/**
	 * @return an array list of 10 players with highest score from database.
	 */
	private ArrayList<User> getHiScrUsersFromDB() {
		ArrayList<User> temp = new ArrayList<>();
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}	
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:user.db");
			Statement statement = connection.createStatement();
		    statement.setQueryTimeout(30); 
		    statement.executeUpdate("CREATE TABLE IF NOT EXISTS user (name STRING, password STRING, type STRING, score INTEGER)");
		    
	        ResultSet resultSet = statement.executeQuery("SELECT * FROM user ORDER BY score DESC");
	        while (resultSet.next() && temp.size() < 10) {
	        	temp.add(new User(resultSet.getString("name"), resultSet.getString("password"), resultSet.getString("type"), resultSet.getInt("score")));
	        }	        
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return temp;
	}
	
	/**
	 * Action when click View Database button.
	 */
	private void viewDBButtActionPerformed(ActionEvent e) {
		frmJustAQuiz.dispose();
		ViewDatabaseFrame view = new ViewDatabaseFrame(currentUser);
		view.start();
	}
	
	/**
	 * Action when click Add Question button.
	 */
	private void addQuestionButtActionPerformed(ActionEvent e) {
		frmJustAQuiz.dispose();
		AddQuestionFrame addQuestionFrame = new AddQuestionFrame(currentUser);
		addQuestionFrame.start();
	}
	
	/**
	 * Set this frame visible
	 */
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmJustAQuiz.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
