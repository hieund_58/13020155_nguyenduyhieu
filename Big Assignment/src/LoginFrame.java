import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import java.awt.Font;
import java.util.ArrayList;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;

import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

/**
 * @author KiLL
 *
 */
public class LoginFrame {

	private JFrame frmLogin;
	private JLabel lblLogo, lblPassword, lblUsername, lblConfirmPassword;
	private JTextField txtUsername;
	private JButton btnLogin, btnQuit;
	private JPasswordField passwordField;
	private JPasswordField passwordFieldConfirm;
	private JButton btnSignUp;
	private JButton btnCancel;
	private JButton btnNewPlayer;

	/**
	 * Create the application.
	 */
	public LoginFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setTitle("Login");
		frmLogin.setBounds(100, 100, 716, 707);
		frmLogin.setLocationRelativeTo(null);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		
		lblLogo = new JLabel("Just a quiz game");
		lblLogo.setToolTipText("fdgfgfdg");
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setForeground(new Color(0, 0, 51, 200));
		lblLogo.setFont(new Font("Gabriola", Font.BOLD, 46));
		lblLogo.setBackground(Color.ORANGE);
		lblLogo.setBounds(0, 27, 696, 44);
		frmLogin.getContentPane().add(lblLogo);
		
		lblUsername = new JLabel("Enter your username");
		lblUsername.setForeground(new Color(0, 0, 153));
		lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsername.setFont(new Font("Papyrus", Font.BOLD | Font.ITALIC, 31));
		lblUsername.setBounds(0, 98, 694, 43);
		frmLogin.getContentPane().add(lblUsername);
		
		txtUsername = new JTextField();
		txtUsername.setText("");
		txtUsername.setHorizontalAlignment(SwingConstants.CENTER);
		txtUsername.setFont(new Font("Segoe Print", Font.BOLD, 29));
		txtUsername.setBounds(206, 152, 289, 56);
		frmLogin.getContentPane().add(txtUsername);
		txtUsername.setColumns(10);
		
		lblPassword = new JLabel("Enter your password\r\n");
		lblPassword.setForeground(new Color(0, 0, 153));
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassword.setFont(new Font("Papyrus", Font.BOLD | Font.ITALIC, 31));
		lblPassword.setBounds(0, 219, 694, 43);
		frmLogin.getContentPane().add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Segoe Print", Font.BOLD, 29));
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		passwordField.setBounds(206, 278, 289, 56);
		frmLogin.getContentPane().add(passwordField);
		
		btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loginButtActionPerformed(e);
			}
		});
		btnLogin.setForeground(new Color(102, 51, 102));
		btnLogin.setFont(new Font("Cambria", Font.BOLD, 22));
		btnLogin.setBounds(261, 391, 175, 55);
		btnLogin.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "continue");
		btnLogin.getActionMap().put("continue", new Action() {
			public void actionPerformed(ActionEvent e) {
				loginButtActionPerformed(e);
			}
			@Override
			public void addPropertyChangeListener(PropertyChangeListener listener) {			
			}
			@Override
			public Object getValue(String key) {
				return null;
			}
			@Override
			public boolean isEnabled() {
				return true;
			}
			@Override
			public void putValue(String key, Object value) {		
			}
			@Override
			public void removePropertyChangeListener(PropertyChangeListener listener) {	
			}
			@Override
			public void setEnabled(boolean b) {		
			}
		});
		frmLogin.getContentPane().add(btnLogin);
		
		btnQuit = new JButton("Quit");
		btnQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmLogin.dispose();
			}
		});
		
		btnNewPlayer = new JButton("New Player");
		btnNewPlayer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNewPlayerActionPerformed(e);
			}
		});
		btnNewPlayer.setForeground(new Color(102, 51, 102));
		btnNewPlayer.setFont(new Font("Cambria", Font.BOLD, 22));
		btnNewPlayer.setBounds(261, 494, 175, 55);
		frmLogin.getContentPane().add(btnNewPlayer);
		btnQuit.setForeground(new Color(102, 51, 102));
		btnQuit.setFont(new Font("Cambria", Font.BOLD, 22));
		btnQuit.setBounds(261, 592, 175, 55);
		frmLogin.getContentPane().add(btnQuit);
		
		lblConfirmPassword = new JLabel("Confirm password\r\n");
		lblConfirmPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfirmPassword.setForeground(new Color(0, 0, 153));
		lblConfirmPassword.setFont(new Font("Papyrus", Font.BOLD | Font.ITALIC, 31));
		lblConfirmPassword.setBounds(2, 345, 694, 43);
		lblConfirmPassword.setVisible(false);
		frmLogin.getContentPane().add(lblConfirmPassword);
		
		passwordFieldConfirm = new JPasswordField();
		passwordFieldConfirm.setHorizontalAlignment(SwingConstants.CENTER);
		passwordFieldConfirm.setFont(new Font("Segoe Print", Font.BOLD, 29));
		passwordFieldConfirm.setBounds(206, 401, 289, 56);
		passwordFieldConfirm.setVisible(false);
		frmLogin.getContentPane().add(passwordFieldConfirm);
		
		btnSignUp = new JButton("Sign Up");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSignUpActionPerformed(e);
			}
		});
		btnSignUp.setForeground(new Color(102, 51, 102));
		btnSignUp.setFont(new Font("Cambria", Font.BOLD, 22));
		btnSignUp.setBounds(261, 494, 175, 55);
		frmLogin.getContentPane().add(btnSignUp);
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnCancelActionPerformed(e);
			}
		});
		btnCancel.setForeground(new Color(102, 51, 102));
		btnCancel.setFont(new Font("Cambria", Font.BOLD, 22));
		btnCancel.setBounds(261, 592, 175, 55);
		frmLogin.getContentPane().add(btnCancel);
	}
	
	/**
	 * Action when click Login button.
	 */
	private void loginButtActionPerformed(ActionEvent e) {
		ArrayList<User> allUsers = User.getAllUsersFromDB();
		for (int i = 0; i < allUsers.size(); i++) {
			if (txtUsername.getText().equals(allUsers.get(i).getName()) && String.valueOf(passwordField.getPassword()).equals(allUsers.get(i).getPassword())) {
				frmLogin.dispose();
				OpeningFrame op = new OpeningFrame(allUsers.get(i));
				op.start();
				break;
			}
		}
		lblUsername.setText("Invalid username or password!");
	}
	
	/**
	 * Action when click New Player button.
	 */
	private void btnNewPlayerActionPerformed(ActionEvent e) {
		lblUsername.setText("Enter your username");
		btnLogin.setVisible(false);
		btnNewPlayer.setVisible(false);
		btnQuit.setVisible(false);
		lblConfirmPassword.setVisible(true);
		passwordFieldConfirm.setVisible(true);
		btnSignUp.setVisible(true);
		btnCancel.setVisible(true);
	}
	
	/**
	 * Action when click Cancel button.
	 */
	private void btnCancelActionPerformed(ActionEvent e) {
		lblUsername.setText("Enter your username");
		lblPassword.setText("Enter your password");
		btnLogin.setVisible(true);
		btnNewPlayer.setVisible(true);
		btnQuit.setVisible(true);
		lblConfirmPassword.setVisible(false);
		passwordFieldConfirm.setVisible(false);
		btnSignUp.setVisible(false);
		btnCancel.setVisible(false);
		txtUsername.setText("");
		passwordField.setText("");
		passwordFieldConfirm.setText("");
		txtUsername.grabFocus();
		txtUsername.requestFocus();
	}
	
	/**
	 * Action when click Sign Up button.
	 */
	private void btnSignUpActionPerformed(ActionEvent e) {
		boolean isQualified = true;
		if (txtUsername.getText().equals("") || String.valueOf(passwordField.getPassword()).equals("") || String.valueOf(passwordFieldConfirm.getPassword()).equals("")) {
			lblUsername.setText("Missing field");
			isQualified = false;
		}
		if (!String.valueOf(passwordField.getPassword()).equals(String.valueOf(passwordFieldConfirm.getPassword()))) {
			lblPassword.setText("Password and confirmation mismatch");
			isQualified = false;
		}
		ArrayList<User> allUsers = User.getAllUsersFromDB();
		for (int i = 0; i < allUsers.size(); i++) {
			if (txtUsername.getText().equals(allUsers.get(i).getName())) {
				lblUsername.setText("Username already exists");
				isQualified = false;
				break;
			}
		}
		if (isQualified) {
			User newUser = new User(txtUsername.getText(), String.valueOf(passwordField.getPassword()));
			newUser.insertToUserDB();
			frmLogin.dispose();
			OpeningFrame op = new OpeningFrame(newUser);
			op.start();
		}
	}
	
	/**
	 * Method to start this frame.
	 */
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrame window = new LoginFrame();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
