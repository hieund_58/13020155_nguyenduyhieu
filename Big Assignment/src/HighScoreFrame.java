import javax.swing.JFrame;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

/**
 * @author KiLL
 *
 */
public class HighScoreFrame {

	private JFrame frmHighScore;
	private User currentUser;
	private ArrayList<User> highestScoreUsers;

	/**
	 * Create the application.
	 */
	/**
	 * @wbp.parser.constructor
	 */
	public HighScoreFrame() {
		initialize();
	}
	
	/**
	 * @param currentUser The current user viewing this frame
	 * @param users The array list of user with highest score
	 */
	public HighScoreFrame(User currentUser, ArrayList<User> users) {
		this.currentUser = new User(currentUser);
		this.highestScoreUsers = new ArrayList<User>();
		this.highestScoreUsers = users;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHighScore = new JFrame();
		frmHighScore.setTitle("High Scores");
		frmHighScore.setBounds(100, 100, 456, 671);
		frmHighScore.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmHighScore.setLocationRelativeTo(null);
		frmHighScore.getContentPane().setLayout(null);
		
		JLabel lblNewLabel_5 = new JLabel("Name");
		lblNewLabel_5.setForeground(new Color(204, 0, 51, 250));
		lblNewLabel_5.setFont(new Font("Lucida Handwriting", Font.BOLD, 19));
		lblNewLabel_5.setBounds(109, 39, 74, 16);
		frmHighScore.getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Best Score ");
		lblNewLabel_6.setForeground(new Color(204, 0, 51, 250));
		lblNewLabel_6.setFont(new Font("Lucida Handwriting", Font.BOLD, 19));
		lblNewLabel_6.setBounds(267, 39, 163, 16);
		frmHighScore.getContentPane().add(lblNewLabel_6);
		
		JLabel scoreLabel1 = new JLabel("");
		scoreLabel1.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel1.setBounds(273, 78, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel1);
		
		JLabel nameLabel1 = new JLabel("");
		nameLabel1.setForeground(new Color(204, 0, 51));
		nameLabel1.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		nameLabel1.setBounds(109, 78, 110, 16);
		frmHighScore.getContentPane().add(nameLabel1);
		
		JLabel scoreLabel2 = new JLabel("");
		scoreLabel2.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel2.setBounds(273, 125, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel2);
		
		JLabel nameLabel2 = new JLabel("");
		nameLabel2.setBounds(109, 125, 110, 16);
		nameLabel2.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		frmHighScore.getContentPane().add(nameLabel2);
		
		JLabel scoreLabel3 = new JLabel("");
		scoreLabel3.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel3.setBounds(273, 172, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel3);
		
		JLabel nameLabel3 = new JLabel("");
		nameLabel3.setBounds(109, 172, 110, 16);
		nameLabel3.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		frmHighScore.getContentPane().add(nameLabel3);
		
		JLabel nameLabel4 = new JLabel("");
		nameLabel4.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		nameLabel4.setBounds(109, 219, 110, 16);
		frmHighScore.getContentPane().add(nameLabel4);
		
		JLabel nameLabel5 = new JLabel("");
		nameLabel5.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		nameLabel5.setBounds(109, 266, 110, 16);
		frmHighScore.getContentPane().add(nameLabel5);
		
		JLabel nameLabel6 = new JLabel("");
		nameLabel6.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		nameLabel6.setBounds(109, 313, 110, 16);
		frmHighScore.getContentPane().add(nameLabel6);
		
		JLabel nameLabel7 = new JLabel("");
		nameLabel7.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		nameLabel7.setBounds(109, 360, 110, 16);
		frmHighScore.getContentPane().add(nameLabel7);
		
		JLabel nameLabel8 = new JLabel("");
		nameLabel8.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		nameLabel8.setBounds(109, 407, 110, 16);
		frmHighScore.getContentPane().add(nameLabel8);
		
		JLabel nameLabel9 = new JLabel("");
		nameLabel9.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		nameLabel9.setBounds(109, 454, 110, 16);
		frmHighScore.getContentPane().add(nameLabel9);
		
		JLabel scoreLabel4 = new JLabel("");
		scoreLabel4.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel4.setBounds(273, 219, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel4);
		
		JLabel scoreLabel5 = new JLabel("");
		scoreLabel5.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel5.setBounds(273, 266, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel5);
		
		JLabel scoreLabel6 = new JLabel("");
		scoreLabel6.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel6.setBounds(273, 313, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel6);
		
		JLabel scoreLabel7 = new JLabel("");
		scoreLabel7.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel7.setBounds(273, 360, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel7);
		
		JLabel scoreLabel8 = new JLabel("");
		scoreLabel8.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel8.setBounds(273, 407, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel8);
		
		JLabel scoreLabel9 = new JLabel("");
		scoreLabel9.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel9.setBounds(273, 454, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel9);
		
		JLabel nameLabel10 = new JLabel("");
		nameLabel10.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		nameLabel10.setBounds(109, 501, 110, 16);
		frmHighScore.getContentPane().add(nameLabel10);
		
		JLabel scoreLabel10 = new JLabel("");
		scoreLabel10.setFont(new Font("Lucida Bright", Font.BOLD, 17));
		scoreLabel10.setBounds(273, 501, 103, 16);
		frmHighScore.getContentPane().add(scoreLabel10);
		
		ArrayList<JLabel> nameLabel = new ArrayList<>();
		nameLabel.add(nameLabel1);
		nameLabel.add(nameLabel2);
		nameLabel.add(nameLabel3);
		nameLabel.add(nameLabel4);
		nameLabel.add(nameLabel5);
		nameLabel.add(nameLabel6);
		nameLabel.add(nameLabel7);
		nameLabel.add(nameLabel8);
		nameLabel.add(nameLabel9);
		nameLabel.add(nameLabel10);
		ArrayList<JLabel> scoreLabel = new ArrayList<>();
		scoreLabel.add(scoreLabel1);
		scoreLabel.add(scoreLabel2);
		scoreLabel.add(scoreLabel3);
		scoreLabel.add(scoreLabel4);
		scoreLabel.add(scoreLabel5);
		scoreLabel.add(scoreLabel6);
		scoreLabel.add(scoreLabel7);
		scoreLabel.add(scoreLabel8);
		scoreLabel.add(scoreLabel9);
		scoreLabel.add(scoreLabel10);
		
		for (int i = 0; i < 10; i++) {
			nameLabel.get(i).setForeground(new Color(204, 0, 51, 250));
			scoreLabel.get(i).setForeground(new Color(204, 0, 51, 250));
		}
		
		if (highestScoreUsers != null) {
			for (int i = 0; i < highestScoreUsers.size(); i++) {
				nameLabel.get(i).setText(highestScoreUsers.get(i).getName());
				scoreLabel.get(i).setText(String.valueOf(highestScoreUsers.get(i).getScore()));
			}
		}
		
		JButton backButton = new JButton("Back to main menu");
		backButton.setFont(new Font("Century Schoolbook", Font.BOLD, 14));
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmHighScore.dispose();
				OpeningFrame op = new OpeningFrame(currentUser);
				op.start();
			}
		});
		backButton.setBounds(123, 544, 193, 69);
		frmHighScore.getContentPane().add(backButton);
		
//		JLabel bgImgLabel = new JLabel("");
//		bgImgLabel.setIcon(new ImageIcon("C:\\Users\\KiLL\\Desktop\\planet_light_spots_space_86643_1920x1080.jpg"));
//		bgImgLabel.setBounds(-20, -376, 553, 1035);
//		frmHighScore.getContentPane().add(bgImgLabel);
	}
	
	/**
	 * Set this frame visible
	 */
	public void start() {
		frmHighScore.setVisible(true);
	}
}
