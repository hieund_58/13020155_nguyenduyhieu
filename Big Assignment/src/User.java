import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class User {
	private String name;
	private String password;
	private String type;
	private int score;
	
	public User() {
		name = password = type = null;
		score = 0;
	}
	
	public User(String name, String password, String type, int score) {
		super();
		this.name = name;
		this.password = password;
		this.type = type;
		this.score = score;
	}
	
	public User(String name, String password, String type) {
		super();
		this.name = name;
		this.password = password;
		this.type = type;
		score = 0;
	}

	public User(String name, String password) {
		super();
		this.name = name;
		this.password = password;
		type = "player";
		score = 0;
	}
	
	public User(User other) {
		this.name = other.name;
		this.password = other.password;
		this.type = other.type;
		this.score = other.score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return " Name: " + name + "\n Password: " + password + "\n Type: " + type + "\n Highest Score: " + score + "\n\n";
	}
	
	/**
	 * @return the connection to User Database
	 */
	public static Connection getUserDBConnection() {
		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:user.db");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} 
		return connection;
	}
	
	/**
	 * Insert the user object who calls this method into User Database
	 */
	public void insertToUserDB() {
		Connection connection = null;
		try {
			connection = getUserDBConnection();
			Statement statement = connection.createStatement();
		    statement.setQueryTimeout(30); 
	        statement.executeUpdate("CREATE TABLE IF NOT EXISTS user (name STRING, password STRING, type STRING, score INTEGER)");
	        ResultSet resultSet = statement.executeQuery("SELECT * FROM user");
	        boolean isAlreadyExist = false;
	        while (resultSet.next()) {
	        	if (this.getName().equals(resultSet.getString("name"))) {
	        		isAlreadyExist = true;
	        		break;
	        	}
	        }
	        if (!isAlreadyExist) {
	        	statement.executeUpdate("INSERT INTO user VALUES('" + this.getName() + "', '" + this.getPassword() + "', '" + this.getType() + "', '" + this.score + "')");
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @param s the score to update. If s is greater than user's current score, score is replaced by s.
	 */
	public void updateScoreInUserDB(int s) {
		Connection connection = null;
		try {
			connection = getUserDBConnection();
			Statement statement = connection.createStatement();
		    statement.setQueryTimeout(30); 
		    
	        statement.executeUpdate("CREATE TABLE IF NOT EXISTS user (name STRING, password STRING, type STRING, score INTEGER)");
	        ResultSet resultSet = statement.executeQuery("SELECT * FROM user WHERE name ='" + this.getName() + "'");
	        if (resultSet.getInt("score") < s) {
	        	statement.executeUpdate("UPDATE user SET score ='" + s + "' WHERE name ='" + this.getName() + "'");
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @return an array list of all user from database
	 */
	public static ArrayList<User> getAllUsersFromDB() {
		ArrayList<User> temp = new ArrayList<>();
		Connection connection = null;
		try {
			connection = getUserDBConnection();
			Statement statement = connection.createStatement();
		    statement.setQueryTimeout(30); 
		    statement.executeUpdate("CREATE TABLE IF NOT EXISTS user (name STRING, password STRING, type STRING, score INTEGER)");
		    
	        ResultSet resultSet = statement.executeQuery("SELECT * FROM user");
	        while (resultSet.next()) {
	        	temp.add(new User(resultSet.getString("name"), resultSet.getString("password"), resultSet.getString("type"), resultSet.getInt("score")));
	        }	        
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return temp;
	}
}
