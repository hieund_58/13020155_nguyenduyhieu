import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.JTextArea;
import java.awt.SystemColor;

/**
 * @author KiLL
 *
 */
public class InGameFrame {

	private JFrame frmQuestions;
	private JTextField answerText;	
	private JLabel questionLabel, optionALabel, optionBLabel, optionCLabel, optionDLabel, yourScoreLabel, answerLabel;
	private JLabel notiLabel;
	private JButton continueButton, answerButton, quitButton;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel questionPanel;
	private JLabel helloPlayerLabel;
	private JTextArea questionTextArea;
	
	private ArrayList<Question> arrListQuestion;
	private int questionIndex = 0;
	private int userScore = 0;
	private User currentUser;
	private boolean isFailed = false;

	/**
	 * Create the application.
	 */
	/**
	 * @wbp.parser.constructor
	 */
	public InGameFrame() {
		initialize();
	}
	
	/**
	 * @param currentUser the current user viewing this frame
	 */
	public InGameFrame(User currentUser) {
		this.currentUser = new User(currentUser);
		initialize();
		
		//QuestionGenerator.insertSampleToQuestionDB();
		QuestionGenerator qg = new QuestionGenerator();
		qg.generateRandomQuestions();
		arrListQuestion = new ArrayList<>(qg.getQuestions());
		
		getQuestionInfo(questionIndex);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmQuestions = new JFrame();
		frmQuestions.setTitle("Questions");
		//frmQuestions.getContentPane().setBackground(new Color(255, 255, 255));
		frmQuestions.setBounds(100, 100, 571, 741);
		frmQuestions.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmQuestions.setLocationRelativeTo(null);
		frmQuestions.getContentPane().setLayout(null);
		
		answerText = new JTextField();
		answerText.setBackground(SystemColor.controlHighlight);
		answerText.setFont(new Font("Trebuchet MS", Font.BOLD, 15));
		answerText.setHorizontalAlignment(SwingConstants.CENTER);
		answerText.setBounds(218, 543, 118, 28);
		frmQuestions.getContentPane().add(answerText);
		frmQuestions.addWindowListener( new WindowAdapter() {
		    public void windowOpened( WindowEvent e ){
		        answerText.requestFocus();
		    }
		}); 
		
		questionPanel = new JPanel();
		//questionPanel.setBackground(new Color(255, 255, 255));
		questionPanel.setOpaque(false);
		questionPanel.setBorder(new TitledBorder(null, "Question 1", TitledBorder.LEADING, TitledBorder.TOP, new Font("Comic Sans MS", Font.BOLD, 14), new Color(59, 59, 59)));
		questionPanel.setBounds(45, 92, 457, 107);
		frmQuestions.getContentPane().add(questionPanel);
		questionPanel.setLayout(null);
		
		questionTextArea = new JTextArea();		
		//questionTextArea.setOpaque(false);
		//questionTextArea.setBackground(new Color(0, 0 , 0, 0));
		questionTextArea.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		//questionTextArea.setBackground(new Color(255, 255, 255));
		questionTextArea.setColumns(3);
		questionTextArea.setEditable(false);
		questionTextArea.setWrapStyleWord(true);
		questionTextArea.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		questionTextArea.setBounds(18, 23, 421, 61);
		questionTextArea.setLineWrap(true);
		questionPanel.add(questionTextArea);
		
		questionLabel = new JLabel("New label");
		questionLabel.setBounds(29, 6, 395, 72);
		questionPanel.add(questionLabel);
		questionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		questionLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 18));
		questionLabel.setBackground(Color.RED);
		
		panel = new JPanel();
		//panel.setBackground(new Color(255, 255, 255));
		panel.setBorder(new TitledBorder(null, "A", TitledBorder.CENTER, TitledBorder.TOP, new Font("Century Schoolbook", Font.BOLD, 14), new Color(59, 59, 59)));
		panel.setBounds(45, 234, 206, 75);
		frmQuestions.getContentPane().add(panel);
		panel.setLayout(null);
		
		optionALabel = new JLabel("New label");
		optionALabel.setFont(new Font("Century Schoolbook", Font.BOLD, 16));
		optionALabel.setBounds(19, 16, 167, 43);
		panel.add(optionALabel);
		optionALabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		panel_1 = new JPanel();
		//panel_1.setBackground(new Color(255, 255, 255));
		panel_1.setBorder(new TitledBorder(null, "B", TitledBorder.CENTER, TitledBorder.TOP, new Font("Century Schoolbook", Font.BOLD, 14), new Color(59, 59, 59)));
		panel_1.setBounds(292, 234, 210, 75);
		frmQuestions.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		optionBLabel = new JLabel("New label");
		optionBLabel.setFont(new Font("Century Schoolbook", Font.BOLD, 16));
		optionBLabel.setBounds(21, 16, 167, 43);
		panel_1.add(optionBLabel);
		optionBLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		panel_2 = new JPanel();
		//panel_2.setBackground(new Color(255, 255, 255));
		panel_2.setBorder(new TitledBorder(null, "C", TitledBorder.CENTER, TitledBorder.TOP, new Font("Century Schoolbook", Font.BOLD, 14), new Color(59, 59, 59)));
		panel_2.setBounds(45, 337, 206, 75);
		frmQuestions.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		optionCLabel = new JLabel("New label");
		optionCLabel.setFont(new Font("Century Schoolbook", Font.BOLD, 16));
		optionCLabel.setBounds(19, 16, 167, 43);
		panel_2.add(optionCLabel);
		optionCLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		panel_3 = new JPanel();
		//panel_3.setBackground(new Color(255, 255, 255));
		panel_3.setBorder(new TitledBorder(null, "D", TitledBorder.CENTER, TitledBorder.TOP, new Font("Century Schoolbook", Font.BOLD, 14), new Color(59, 59, 59)));
		panel_3.setBounds(292, 337, 210, 75);
		frmQuestions.getContentPane().add(panel_3);
		panel_3.setLayout(null);
		
		optionDLabel = new JLabel("New label");
		optionDLabel.setFont(new Font("Century Schoolbook", Font.BOLD, 16));
		optionDLabel.setBounds(21, 16, 167, 43);
		panel_3.add(optionDLabel);
		optionDLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		answerLabel = new JLabel("Your answer (A/B/C/D)");
		answerLabel.setFont(new Font("Andalus", Font.BOLD, 16));
		answerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		answerLabel.setBounds(0, 506, 555, 25);
		frmQuestions.getContentPane().add(answerLabel);
		
		yourScoreLabel = new JLabel("New label");
		yourScoreLabel.setFont(new Font("Andalus", Font.BOLD, 23));
		yourScoreLabel.setForeground(new Color(0, 0, 0, 230));
		yourScoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
		yourScoreLabel.setBounds(0, 420, 555, 48);
		yourScoreLabel.setText("Your score is now " + 0 );
		frmQuestions.getContentPane().add(yourScoreLabel);
		
		answerButton = new JButton("Answer!");
		answerButton.setFont(new Font("Candara", Font.BOLD, 14));
		answerButton.setBounds(218, 584, 118, 43);
		answerButton.setEnabled(true);
		answerButton.setVisible(true);
		answerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				answerButtonActionPerformed(e);
				}
			});	
	    answerButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "answer");
		answerButton.getActionMap().put("answer", new Action() {
			public void actionPerformed(ActionEvent e) {
				answerButtonActionPerformed(e);
			}
			@Override
			public void addPropertyChangeListener(PropertyChangeListener listener) {			
			}
			@Override
			public Object getValue(String key) {
				return null;
			}
			@Override
			public boolean isEnabled() {
				return true;
			}
			@Override
			public void putValue(String key, Object value) {		
			}
			@Override
			public void removePropertyChangeListener(PropertyChangeListener listener) {	
			}
			@Override
			public void setEnabled(boolean b) {		
			}
		});
		frmQuestions.getContentPane().add(answerButton);
		
		quitButton = new JButton("Quit");
		quitButton.setFont(new Font("Candara", Font.BOLD, 14));
		quitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quitButtActionPerformed(e);
			}
		});
		quitButton.setBounds(218, 638, 118, 43);
		frmQuestions.getContentPane().add(quitButton);		
		
		notiLabel = new JLabel("Noti Label");
		notiLabel.setForeground(new Color(255, 0, 0, 230));
		notiLabel.setFont(new Font("Kristen ITC", Font.BOLD, 25));
		notiLabel.setHorizontalAlignment(SwingConstants.CENTER);
		notiLabel.setBounds(0, 495, 555, 36);
		notiLabel.setVisible(false);
		frmQuestions.getContentPane().add(notiLabel);
		
		continueButton = new JButton("Continue ");
		continueButton.setFont(new Font("Candara", Font.BOLD, 14));
		continueButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contButtActionPerformed(e);
			}
		});
	    continueButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "continue");
		continueButton.getActionMap().put("continue", new Action() {
			public void actionPerformed(ActionEvent e) {
				contButtActionPerformed(e);
			}
			@Override
			public void addPropertyChangeListener(PropertyChangeListener listener) {			
			}
			@Override
			public Object getValue(String key) {
				return null;
			}
			@Override
			public boolean isEnabled() {
				return true;
			}
			@Override
			public void putValue(String key, Object value) {		
			}
			@Override
			public void removePropertyChangeListener(PropertyChangeListener listener) {	
			}
			@Override
			public void setEnabled(boolean b) {		
			}
		});
	    continueButton.setBounds(218, 584, 118, 43);
		continueButton.setVisible(false);
		frmQuestions.getContentPane().add(continueButton);
		
		helloPlayerLabel = new JLabel("New label");
		helloPlayerLabel.setForeground(new Color(0, 0, 51));
		helloPlayerLabel.setFont(new Font("Kristen ITC", Font.BOLD | Font.ITALIC, 16));
		helloPlayerLabel.setBounds(26, 23, 478, 43);
		helloPlayerLabel.setText("<html>Hello " + currentUser.getName() + ", each true answer will have you 1 score<br>There are 15 questions</html>");
		frmQuestions.getContentPane().add(helloPlayerLabel);	
		
//		JLabel bgImageLabel = new JLabel("");
//		bgImageLabel.setIcon(new ImageIcon("C:\\Users\\KiLL\\Desktop\\BSP_054.jpg"));
//		bgImageLabel.setBounds(-48, -26, 632, 723);
//		frmQuestions.getContentPane().add(bgImageLabel);
	}
	
	//Action upon clicking Answer Button
	private void answerButtonActionPerformed(ActionEvent e) {		
		int n = JOptionPane.showConfirmDialog(
                 frmQuestions, "Are you sure your choice is " + answerText.getText().toUpperCase() + "?",
                 "Confirm",
                 JOptionPane.YES_NO_OPTION);
		//Confirm yes, this is my final answer 
		if (n == JOptionPane.YES_OPTION) {     
			answerButton.setVisible(false);
			answerLabel.setVisible(false);
			answerText.setVisible(false);
			//True answer: score += 1, update GUI elements
			if (answerText.getText().equalsIgnoreCase(arrListQuestion.get(questionIndex).getAnswer())) {
				userScore += 1;
				currentUser.setScore(userScore);
				yourScoreLabel.setText("Your score is now " + userScore);	
				notiLabel.setVisible(true);
				notiLabel.setText("Your answer " + answerText.getText().toUpperCase() + " is correct!");
				continueButton.setVisible(true);
				//True answer to the last question: update user score in database, open a new End Game frame to celebrate
				if (questionIndex == arrListQuestion.size() - 1) {
					currentUser.updateScoreInUserDB(userScore);
					frmQuestions.dispose();
					EndGameFrame cf = new EndGameFrame(currentUser, isFailed);
					cf.start();
				}
			}
			//Wrong answer: set final score to update later (when clicking quit)
			else {
				currentUser.setScore(userScore);
				isFailed = true;
				notiLabel.setVisible(true);
				notiLabel.setText("Your answer is wrong! " + arrListQuestion.get(questionIndex).getAnswer() + " is correct.");
				answerButton.setVisible(false);		
			}
			answerText.setText("");
		}
		//Confirm no
		else {
			// Do nothing
		}
	}
	
	//Action upon clicking Continue Button
	private void contButtActionPerformed(ActionEvent e) {
		answerButton.setVisible(true);
		answerLabel.setVisible(true);
		answerText.setVisible(true);
		answerText.grabFocus();
		answerText.requestFocus();
		notiLabel.setVisible(false);
		continueButton.setVisible(false);
		//Get info of next question
		try {
			if (questionIndex < arrListQuestion.size() - 1) {
				questionIndex++;
				getQuestionInfo(questionIndex);	
			}
		} catch (IndexOutOfBoundsException ex) {
			ex.printStackTrace();
		}
		int questionNum = questionIndex + 1;
		questionPanel.setBorder(new TitledBorder(null, "Question " + questionNum, TitledBorder.LEADING, TitledBorder.TOP, new Font("Comic Sans MS", Font.BOLD, 14), null));
	}
	
	//Parse info of question i to GUI elements
	private void getQuestionInfo(int questIndex) {
		questionLabel.setText(arrListQuestion.get(questIndex).getQuestion());
		questionTextArea.setText(arrListQuestion.get(questIndex).getQuestion());
		optionALabel.setText(arrListQuestion.get(questIndex).getOptionA());
		optionBLabel.setText(arrListQuestion.get(questIndex).getOptionB());
		optionCLabel.setText(arrListQuestion.get(questIndex).getOptionC());
		optionDLabel.setText(arrListQuestion.get(questIndex).getOptionD());
	}
	
	//Action upon clicking Quit Button
	private void quitButtActionPerformed(ActionEvent e) {
		//Game Over: update score, open an End Game Frame to say sorry
		if (isFailed) {
			currentUser.updateScoreInUserDB(userScore);;
			frmQuestions.dispose();
			EndGameFrame cf = new EndGameFrame(currentUser, isFailed);
			cf.start();
		}
		else {
			int n = JOptionPane.showConfirmDialog(
	                frmQuestions, "<html>Are you sure your want to quit? <br> You will keep your score of " + userScore + "</html>",
	                "Confirm",
	                JOptionPane.YES_NO_OPTION);
			//Confirm quit: keep current score and update in DB, back to Opening Frame
			if (n == JOptionPane.YES_OPTION) {
				currentUser.updateScoreInUserDB(userScore);
				frmQuestions.dispose();
				OpeningFrame op = new OpeningFrame(currentUser);
				op.start();
			}
			else {
				//Do nothing
			}
		}
	}
	
	/**
	 * Set this frame visible
	 */
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmQuestions.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}

