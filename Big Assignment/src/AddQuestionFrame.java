import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author KiLL
 *
 */
public class AddQuestionFrame {

	private JFrame frmAddQuestion;
	private User user;
	private JTextField txtLevel;
	private JTextField txtQuestion;
	private JTextField txtOptionA;
	private JTextField txtOptionB;
	private JTextField txtOptionC;
	private JTextField txtOptionD;
	private JTextField txtAnswer;
	private JLabel lblNoti;

	/**
	 * Create the application.
	 */
	/**
	 * @wbp.parser.constructor
	 */
	public AddQuestionFrame() {
		initialize();
	}
	
	/**
	 * @param currentUser the current user viewing this frame
	 */
	public AddQuestionFrame(User currentUser) {
		this.user = new User(currentUser);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAddQuestion = new JFrame();
		frmAddQuestion.setTitle("Add Question");
		frmAddQuestion.setBounds(100, 100, 712, 540);
		frmAddQuestion.setLocationRelativeTo(null);
		frmAddQuestion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAddQuestion.getContentPane().setLayout(null);
		
		JLabel lblPleaseFillIn = new JLabel("Please fill in");
		lblPleaseFillIn.setBounds(0, 11, 696, 76);
		lblPleaseFillIn.setHorizontalAlignment(SwingConstants.CENTER);
		lblPleaseFillIn.setForeground(new Color(0, 0, 153));
		lblPleaseFillIn.setFont(new Font("Papyrus", Font.BOLD | Font.ITALIC, 31));
		frmAddQuestion.getContentPane().add(lblPleaseFillIn);
		
		JLabel lblLevel = new JLabel("Level (1-15)");
		lblLevel.setForeground(new Color(0, 0, 0, 230));
		lblLevel.setFont(new Font("Andalus", Font.BOLD, 20));
		lblLevel.setBounds(33, 82, 124, 51);
		frmAddQuestion.getContentPane().add(lblLevel);
		
		JLabel lblQuestion = new JLabel("Question");
		lblQuestion.setForeground(new Color(0, 0, 0, 230));
		lblQuestion.setFont(new Font("Andalus", Font.BOLD, 18));
		lblQuestion.setBounds(33, 144, 97, 51);
		frmAddQuestion.getContentPane().add(lblQuestion);
		
		JLabel lblOptionA = new JLabel("Option A");
		lblOptionA.setForeground(new Color(0, 0, 0, 230));
		lblOptionA.setFont(new Font("Andalus", Font.BOLD, 18));
		lblOptionA.setBounds(33, 206, 97, 51);
		frmAddQuestion.getContentPane().add(lblOptionA);
		
		JLabel lblOptionB = new JLabel("Option C");
		lblOptionB.setForeground(new Color(0, 0, 0, 230));
		lblOptionB.setFont(new Font("Andalus", Font.BOLD, 18));
		lblOptionB.setBounds(33, 268, 97, 51);
		frmAddQuestion.getContentPane().add(lblOptionB);
		
		JLabel lblOptionC = new JLabel("Option B");
		lblOptionC.setForeground(new Color(0, 0, 0, 230));
		lblOptionC.setFont(new Font("Andalus", Font.BOLD, 18));
		lblOptionC.setBounds(362, 206, 97, 51);
		frmAddQuestion.getContentPane().add(lblOptionC);
		
		JLabel lblOptionD = new JLabel("Option D");
		lblOptionD.setForeground(new Color(0, 0, 0, 230));
		lblOptionD.setFont(new Font("Andalus", Font.BOLD, 18));
		lblOptionD.setBounds(362, 268, 97, 51);
		frmAddQuestion.getContentPane().add(lblOptionD);
		
		JLabel lblAnswer = new JLabel("Answer");
		lblAnswer.setForeground(new Color(0, 0, 0, 230));
		lblAnswer.setFont(new Font("Andalus", Font.BOLD, 18));
		lblAnswer.setBounds(33, 330, 97, 51);
		frmAddQuestion.getContentPane().add(lblAnswer);
		
		txtLevel = new JTextField();
		txtLevel.setFont(new Font("Andalus", Font.BOLD, 20));
		txtLevel.setBounds(155, 94, 86, 27);
		frmAddQuestion.getContentPane().add(txtLevel);
		txtLevel.setColumns(10);
		
		txtQuestion = new JTextField();
		txtQuestion.setFont(new Font("Andalus", Font.BOLD, 20));
		txtQuestion.setBounds(155, 156, 507, 27);
		frmAddQuestion.getContentPane().add(txtQuestion);
		txtQuestion.setColumns(10);
		
		txtOptionA = new JTextField();
		txtOptionA.setFont(new Font("Andalus", Font.BOLD, 20));
		txtOptionA.setBounds(155, 218, 168, 27);
		frmAddQuestion.getContentPane().add(txtOptionA);
		txtOptionA.setColumns(10);
		
		txtOptionB = new JTextField();
		txtOptionB.setFont(new Font("Andalus", Font.BOLD, 20));
		txtOptionB.setBounds(479, 218, 183, 27);
		frmAddQuestion.getContentPane().add(txtOptionB);
		
		txtOptionC = new JTextField();
		txtOptionC.setFont(new Font("Andalus", Font.BOLD, 20));
		txtOptionC.setBounds(155, 280, 168, 27);
		frmAddQuestion.getContentPane().add(txtOptionC);
		
		txtOptionD = new JTextField();
		txtOptionD.setFont(new Font("Andalus", Font.BOLD, 20));
		txtOptionD.setBounds(479, 280, 183, 27);
		frmAddQuestion.getContentPane().add(txtOptionD);
		
		txtAnswer = new JTextField();
		txtAnswer.setFont(new Font("Andalus", Font.BOLD, 20));
		txtAnswer.setBounds(155, 342, 71, 27);
		frmAddQuestion.getContentPane().add(txtAnswer);
		
		JButton btnBack = new JButton("Back ");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAddQuestion.dispose();
				OpeningFrame openingFrame = new OpeningFrame(user);
				openingFrame.start();
			}
		});
		btnBack.setFont(new Font("Century Schoolbook", Font.BOLD, 14));
		btnBack.setBounds(413, 404, 149, 58);
		frmAddQuestion.getContentPane().add(btnBack);
		
		JButton btnAddQuestion = new JButton("Add Question");
		btnAddQuestion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddQuestionActionPerformed(e);
			}
		});
		btnAddQuestion.setFont(new Font("Century Schoolbook", Font.BOLD, 14));
		btnAddQuestion.setBounds(132, 404, 149, 58);
		frmAddQuestion.getContentPane().add(btnAddQuestion);
		
		lblNoti = new JLabel("Noti");
		lblNoti.setForeground(Color.RED);
		lblNoti.setFont(new Font("Lucida Sans", Font.BOLD | Font.ITALIC, 19));
		lblNoti.setBounds(322, 342, 340, 27);
		lblNoti.setVisible(false);
		frmAddQuestion.getContentPane().add(lblNoti);
	}
	
	/**
	 * Action when click Add Question button.
	 */
	private void btnAddQuestionActionPerformed(ActionEvent e) {
		if (txtLevel.getText().equals("") || txtQuestion.getText().equals("") || txtOptionA.getText().equals("") 
			|| txtOptionB.getText().equals("") || txtOptionC.getText().equals("") || txtOptionD.getText().equals("")
			|| txtAnswer.getText().equals("")) {
			lblNoti.setText("Missing field!");
			lblNoti.setVisible(true);
		}
		else if (txtLevel.getText().contains("'") || txtQuestion.getText().contains("'") || txtOptionA.getText().contains("'") 
			|| txtOptionB.getText().contains("'") || txtOptionC.getText().contains("'") || txtOptionD.getText().contains("'")
			|| txtAnswer.getText().contains("'")) {
			lblNoti.setText("Fields can't contain the character '");
			lblNoti.setVisible(true);
		}
		else if (!txtAnswer.getText().equalsIgnoreCase("A") && !txtAnswer.getText().equalsIgnoreCase("B")
				&& !txtAnswer.getText().equalsIgnoreCase("C") && !txtAnswer.getText().equalsIgnoreCase("D")) {
			lblNoti.setText("Answer is invalid!");
			lblNoti.setVisible(true);
		}
		else if (!isInt(txtLevel.getText())) {
			lblNoti.setText("Level is invalid!");
			lblNoti.setVisible(true);
		}
		else {
			int level = Integer.parseInt(txtLevel.getText());
			if (level <= 0 || level > 15) {
				lblNoti.setText("Level is invalid!");
				lblNoti.setVisible(true);
			}
			else {
				Question q = new Question(level, txtQuestion.getText(), txtOptionA.getText(),
										  txtOptionB.getText(), txtOptionC.getText(), txtOptionD.getText(), txtAnswer.getText());
				q.insertToQuestionDB();
				lblNoti.setText("Added one question to Database!");
				lblNoti.setVisible(true);
				txtLevel.setText("");
				txtQuestion.setText("");
				txtOptionA.setText("");
				txtOptionB.setText("");
				txtOptionC.setText("");
				txtOptionD.setText("");
				txtAnswer.setText("");
			}
		}
		txtLevel.grabFocus();
		txtLevel.requestFocus();
	}
	
	/**
	 * @param strNum the string to check if it could be converted to integer.
	 * @return boolean value if strNum could be converted or not.
	 */
	private static boolean isInt(String strNum) {
	    boolean ret = true;
	    try {
	        Integer.parseInt(strNum);
	    }catch (NumberFormatException e) {
	        ret = false;
	    }
	    return ret;
	}
	
	/**
	 * Set this frame visible
	 */
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmAddQuestion.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
