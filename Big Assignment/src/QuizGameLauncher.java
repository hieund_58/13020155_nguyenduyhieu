
/**
 * @author KiLL
 * @version 1.0
 * Main class to run the program.
 */
public class QuizGameLauncher {
	/**
	 * @param args ignored
	 */
	public static void main(String[] args) {
		User user1 = new User("admin", "admin", "admin");
		User user2 = new User("abc", "123", "player");
		user1.insertToUserDB();
		user2.insertToUserDB();
		QuestionGenerator.insertSampleToQuestionDB();
		LoginFrame loginFrame = new LoginFrame();
		loginFrame.start();
	}
}
