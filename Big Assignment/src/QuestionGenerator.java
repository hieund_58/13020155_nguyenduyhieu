import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * @author KiLL
 *
 */
public class QuestionGenerator {
	
	private ArrayList<Question> questions;
	
	/**
	 * @return an array list of question
	 */
	public ArrayList<Question> getQuestions() {
		return questions;
	}

	/**
	 * @param questions the array list to set
	 */
	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	}
	
	/**
	 * Generate an array list of 15 random questions from Question Database.
	 */
	public void generateRandomQuestions() {
		questions = new ArrayList<Question>(15);
		Random rand = new Random(System.currentTimeMillis());
		//iterate for 15 levels, each level choose a random question from the bank
		for (int i = 1; i <= 15; i++) {
			//random number from 0 to size - 1 'cause each array list has size() questions
			int randomNum = rand.nextInt((Question.getAllQuestionsFromDB().get(i).size() - 1 - 0) + 1) + 0;
			questions.add(Question.getAllQuestionsFromDB().get(i).get(randomNum));
		}
	}
	
	/**
	 * Create question database and insert samples for the first run
	 */
	public static void insertSampleToQuestionDB() {
		Connection connection = null;
		try {
			connection = Question.getQuestionDBConnection();
			Statement statement = connection.createStatement();
		    statement.setQueryTimeout(30);     
	        statement.executeUpdate("CREATE TABLE IF NOT EXISTS question (level INTEGER, question STRING, optiona STRING, optionb STRING, optionc STRING, optiond STRING, answer STRING)");
	        ResultSet resultSet = statement.executeQuery("SELECT * FROM question");	        
	        if (!resultSet.next()) {
	        	for (int i = 1; i <= 15; i++ ) {
	        		for (Question q : SampleQuestionBank().get(i)) {
	        			statement.executeUpdate("INSERT INTO question VALUES('" + q.getLevel() + "', '" + q.getQuestion() + "', '" + q.getOptionA() + "', '" + q.getOptionB() + "', '" + q.getOptionC() + "', '" + q.getOptionD() + "', '" + q.getAnswer() + "')");
	        		}
	        	}      	
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static HashMap<Integer, ArrayList<Question>> SampleQuestionBank() {
		//Hash map stores same level questions in each array list 
		//Question are at https://www.gamefaqs.com/gba/919785-who-wants-to-be-a-millionaire-2nd-edition/faqs/40044
		HashMap<Integer, ArrayList<Question>> hm = new HashMap<>();
		hm.put(1, new ArrayList<Question>());
		hm.put(2, new ArrayList<Question>());
		hm.put(3, new ArrayList<Question>());
		hm.put(4, new ArrayList<Question>());
		hm.put(5, new ArrayList<Question>());
		hm.put(6, new ArrayList<Question>());
		hm.put(7, new ArrayList<Question>());
		hm.put(8, new ArrayList<Question>());
		hm.put(9, new ArrayList<Question>());
		hm.put(10, new ArrayList<Question>());
		hm.put(11, new ArrayList<Question>());
		hm.put(12, new ArrayList<Question>());
		hm.put(13, new ArrayList<Question>());
		hm.put(14, new ArrayList<Question>());
		hm.put(15, new ArrayList<Question>());
		
		ArrayList<Question> lvl1_questions = new ArrayList<>();
		Question lvl1_q1 = new Question(1,"Which disease devastated livestock across the UK during 2001?");
		lvl1_q1.setOptions("Hand-and-foot", "Foot-in-mouth", "Hand-to-mouth", "Foot-and-mouth");
		lvl1_q1.setAnswer("D");
		Question lvl1_q2 = new Question(1,"Which of these kills its victims by constriction?");
		lvl1_q2.setOptions("Andalucia", "Anaconda", "Andypandy", "Annerobinson");
		lvl1_q2.setAnswer("B");
		Question lvl1_q3 = new Question(1,"Which of these might be used in underwater naval operations?");
		lvl1_q3.setOptions("Frogmen", "Newtmen", "Toadmen", "Tadpolemen");
		lvl1_q3.setAnswer("A");
		Question lvl1_q4 = new Question(1,"In the UK, VAT stands for value-added ...?");
		lvl1_q4.setOptions("Transaction", "Total", "Tax", "Trauma");
		lvl1_q4.setAnswer("C");
		Question lvl1_q5 = new Question(1,"What are you said to do to a habit when you break it?");
		lvl1_q5.setOptions("Throw it", "Punch it", "Kick it", "Eat it");
		lvl1_q5.setAnswer("C");
		lvl1_questions.add(lvl1_q1);
		lvl1_questions.add(lvl1_q2);
		lvl1_questions.add(lvl1_q3);
		lvl1_questions.add(lvl1_q4);
		lvl1_questions.add(lvl1_q5);
		hm.put(1, lvl1_questions);
		
		//hm.get(2).add(new Question(2, "", "", "", "", "", ""));
		hm.get(2).add(new Question(2, "Where do you proverbially wear your heart, if you show your true feelings?", "On your collar", "On your lapel", "On your cuff", "On your sleeve", "D"));
		hm.get(2).add(new Question(2, "What might an electrician lay?", "Tables", "Gables", "Cables", "Stables", "C"));
		hm.get(2).add(new Question(2, "What would a tattie picker harvest?", "Raspberries", "Corn", "Potatoes", "Apples", "C"));
		hm.get(2).add(new Question(2, "How is a play on words commonly described?", "Pan", "Pin", "Pen", "Pun", "D"));
		hm.get(2).add(new Question(2, "Which colour is used as a term to describe an illegal market in rare goods?", "Blue", "Red", "Black", "White", "C"));
		
		//hm.get(3).add(new Question(3, "", "", "", "", "", ""));
		hm.get(3).add(new Question(3, "Which character was first played by Arnold Schwarzenegger in a 1984 film?", "Demonstrator", "Instigator", "Investigator", "Terminator", "D"));
		hm.get(3).add(new Question(3, "Which of these would a film actor like to receive?", "Oliver", "Oscar", "Oliphant", "Osbert", "B"));
		hm.get(3).add(new Question(3, "In which country would you expect to be greeted with the word <bonjour>?", "Italy", "France", "Spain", "Wales", "B"));
		hm.get(3).add(new Question(3, "Which country is not an island?", "Madagascar", "Cuba", "Germany", "Jamaica", "C"));
		hm.get(3).add(new Question(3, "What name is given to a playing card with a single symbol on it?", "Whizz", "Hotshot", "Ace", "Star", "C"));
		
		//hm.get(4).add(new Question(4, "", "", "", "", "", ""));
		hm.get(4).add(new Question(4, "Which of these is a tool for shaping and smoothing wood?", "Train", "Plane", "Car", "Bike", "B"));
		hm.get(4).add(new Question(4, "What do the Americans call what we call sweets?", "Randy", "Dandy", "Sandy", "Candy", "D"));
		hm.get(4).add(new Question(4, "What would you expect to see at the London Aquarium?", "Flowers", "Trees", "Steam rollers", "Fish", "D"));
		hm.get(4).add(new Question(4, "According to the old adage, how many lives does a cat have?", "Five", "Seven", "Nine", "Ten", "C"));
		hm.get(4).add(new Question(4, "Which of these is a keyboard instrument?", "Harpsichord", "Ripcord", "Pyjama cord", "Sashcord", "A"));
		
		//hm.get(5).add(new Question(5, "", "", "", "", "", ""));
		hm.get(5).add(new Question(5, "How many moons orbit the Earth?", "One", "Two", "Three", "Four", "A"));
		hm.get(5).add(new Question(5, "By what abbreviation is a compact disc commonly known?", "CD", "COD", "CDIS", "COMPD", "A"));
		hm.get(5).add(new Question(5, "Which country shares a land border with the UK?", "Portugal", "Libya", "Vietnam", "Ireland", "D"));
		hm.get(5).add(new Question(5, "The star sign Aquarius is also known as what?", "The Water-carrier", "The Food-carrier", "The Hod-carrier", "The Bag-carrier", "A"));
		hm.get(5).add(new Question(5, "What are said to be down when things are not going well?", "Egg", "Bacon", "Chips", "Beans", "C"));
		
		hm.get(6).add(new Question(6, "What name is given to a mound or ridge of windblown sand?", "Drone", "Dude", "Dime", "Dune", "D"));
		hm.get(6).add(new Question(6, "Who has the authority to change a ball during a football match?", "Sky Sports", "The home team", "Alex Ferguson", "The referee", "D"));
		hm.get(6).add(new Question(6, "Which of these applies to the shape of a soccer ball?", "Conical", "Cylindrical", "Spherical", "Oval", "C"));
		hm.get(6).add(new Question(6, "Which activity would you most associate with a mole?", "Burrowing", "Climbing", "Swimming", "Flying", "A"));
		hm.get(6).add(new Question(6, "Which is not a type of antelope?", "Gorilla", "Gerenuk", "Gemsbok", "Gnu", "A"));
		
		hm.get(7).add(new Question(7, "Which is another name for a short melodious tune?", "Oxygen", "Air", "Nitrogen", "Stratosphere", "B"));
		hm.get(7).add(new Question(7, "Which of these is a large woodwind instrument?", "Buffoon", "Pantaloon", "Bassoon", "Macaroon", "C"));
		hm.get(7).add(new Question(7, "In which of the following might food be stored?", "Larder", "Shed", "Greenhouse", "Garage", "A"));
		hm.get(7).add(new Question(7, "Which of these geographical features is a mountain?", "Kilimanjaro", "Danube", "Amazon", "Nile", "A"));
		hm.get(7).add(new Question(7, "Which of these is a person who performs tricks that deceive the eye?", "Illustrator", "Illustrator", "Illiterate", "Illusionist", "D"));
		
		hm.get(8).add(new Question(8, "Something mediocre can be described as no great ...?", "Shakes", "Quivers", "Wobbles", "Trembles", "A"));
		hm.get(8).add(new Question(8, "What do you proverbially let down when behaving without reserve?", "Shoulders", "Elbows", "Knees", "Hair", "D"));
		hm.get(8).add(new Question(8, "With which football club was David Beckhams name not linked in 2003?", "Barcelona", "Scunthorpe United", "AC Milan", "Real Madrid", "B"));
		hm.get(8).add(new Question(8, "Which of these is a slang term for a mean person?", "Cheapskate", "Cheapshark", "Cheapmackerel", "Cheaphaddock", "A"));
		hm.get(8).add(new Question(8, "Which of these is a type of beer?", "Acid", "Bitter", "Tart", "Sour", "B"));
		
		hm.get(9).add(new Question(9, "Which of these is an ice cream dessert?", "Sundae", "Mondae", "Tuesdae", "Wednesdae", "A"));
		hm.get(9).add(new Question(9, "Which of these is a popular garden flower?", "Busnation", "Carnation", "Trainnation", "Planenation", "B"));
		hm.get(9).add(new Question(9, "What is the name of the instrument panel in a car?", "Chargeboard", "Sprintboard", "Dashboard", "Jogboard", "C"));
		hm.get(9).add(new Question(9, "Which part of the body do bronchial infections mainly attack?", "Eyes", "Liver", "Spleen", "Lungs", "D"));
		hm.get(9).add(new Question(9, "Which of the following is not a geological period?", "Jurassic", "Palaeozoic", "Triassic", "Boracic", "D"));
		
		hm.get(10).add(new Question(10, "Which of these is a type of drum?", "Bear", "Snare", "Fair", "Blair", "B"));
		hm.get(10).add(new Question(10, "What copy can be said to describe something identical?", "Oxygen", "Hydrogen", "Nitrogen", "Carbon", "D"));
		hm.get(10).add(new Question(10, "Which of these is a spicy Indian dish?", "Spaghetti", "Biriani", "Bellini", "Crostini", "B"));
		hm.get(10).add(new Question(10, "Which of these is a type of cured herring?", "Floater", "Bloater", "Gloater", "Eloper", "B"));
		hm.get(10).add(new Question(10, "Which country is not crossed by the Arctic Circle?", "Norway", "Finland", "Greece", "Sweden", "C"));
		
		hm.get(11).add(new Question(11, "The West Country is famous for which alcoholic drink", "Wine", "Cider", "Beer", "Whisky", "B"));
		hm.get(11).add(new Question(11, "Which is not a recognized playing surface for tennis?", "Grass", "Linoleum", "Clay", "Cement", "B"));
		hm.get(11).add(new Question(11, "Which of the following is a small sausage?", "Maraschino", "Chipolata", "Risotto", "Gazpacho", "B"));
		hm.get(11).add(new Question(11, "Which of these is a term meaning a match in bridge?", "Pencil", "Eraser", "Rubber", "Crayon", "C"));
		hm.get(11).add(new Question(11, "Which of these is a carnivorous plant?", "Mars anteater", "Saturn spidernet", "Pluto bugsnare", "Venus flytrap", "D"));
		
		hm.get(12).add(new Question(12, "Which of these are located at the back of your throat?", "Deltoids", "Tabloids", "Adenoids", "Solenoids", "C"));
		hm.get(12).add(new Question(12, "What type of steroids have a protein-building effect?", "Carbolic", "Shambolic", "Anabolic", "Diabolic", "C"));
		hm.get(12).add(new Question(12, "Which is not a classification of star?", "Red giant", "White dwarf", "Red dwarf", "Green goblin", "D"));
		hm.get(12).add(new Question(12, "An achievement to be proud of is a  ... in ones cap?", "Quill", "Wing", "Plume", "Feather", "D"));
		hm.get(12).add(new Question(12, "Which of these is a term for an actor?", "Equestrian", "Thespian", "Pedestrian", "Martian", "B"));
		
		hm.get(13).add(new Question(13, "Who was not a Roman emperor?", "Hadrian", "Parallax", "Claudius", "Tiberius", "B"));
		hm.get(13).add(new Question(13, "What is the French for king?", "Kaiser", "Duc", "Herzog", "Roi", "D"));
		hm.get(13).add(new Question(13, "According to the saying, there is no such thing as a free ...?", "Laugh", "Lager", "Lunch", "Loan", "C"));
		hm.get(13).add(new Question(13, "The Latin phrase <compos mentis> means <of sound ...?>", "Advice", "Health", "Mind", "Garden", "C"));
		hm.get(13).add(new Question(13, "Which of the following is a form of poker?", "Stud", "Playboy", "Gigolo", "Lothario", "A"));
		
		hm.get(14).add(new Question(14, "Which law states that if anything can go wrong, it will?", "Parkinson s Law", "Boyle s Law", "Murphy s Law", "Cole s Law", "C"));
		hm.get(14).add(new Question(14, "Which of these animals does not represent a sign of the zodiac?", "Bull", "Ram", "Goat", "Donkey", "D"));
		hm.get(14).add(new Question(14, "In which country is the Atacama Desert?", "Mexico", "Morocco", "Chile", "Mongolia", "C"));
		hm.get(14).add(new Question(14, "Vesper is an archaic term for which time of day?", "Morning", "Evening", "Noon", "Night", "B"));
		hm.get(14).add(new Question(14, "Eglantine is a species of which plant?", "Ivy", "Hollyhock", "Rose", "Iris", "C"));
		
		hm.get(15).add(new Question(15, "In sumo wrestling, what is the dohyo?", "Referee", "Code of practice", "Champion", "Ring", "D"));
		hm.get(15).add(new Question(15, "To which group of Greek islands does Kalymnos belong?", "Cyclades", "Dodecanese", "Ionian", "Sporades", "B"));
		hm.get(15).add(new Question(15, "To which of these colours does the word <cerulean> relate?", "Red", "Yellow", "White", "Blue", "D"));
		hm.get(15).add(new Question(15, "Singultus is the medical term for what?", "Sneeze", "Cough", "Yawn", "Hiccup", "D"));
		hm.get(15).add(new Question(15, "In ancient Persia, what was a magus?", "Teacher", "Soothsayer", "Warlord", "Priest", "D"));
		
		return hm;
	}
}
