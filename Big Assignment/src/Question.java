import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author KiLL
 *
 */
public class Question {
	private String question, answer;
	private String optionA, optionB, optionC, optionD;
	private int level;
	
	/**
	 * Default constructor
	 */
	public Question() {
		question = answer = null;
		optionA = optionB = optionC = optionD = null;	
		level = 0;
	}

	/**
	 * Full attributes constructor
	 * @param level the level of this question
	 * @param question the question text of this question	
	 * @param optionA option A of this question
	 * @param optionB option B of this question
	 * @param optionC option C of this question
	 * @param optionD option D of this question
	 * @param answer the answer (A/B/C/D) of this question
	 */
	public Question(int level, String question, String optionA, String optionB, String optionC, String optionD, String answer) {
		this.setLevel(level);
		this.question = question;
		this.optionA = optionA;
		this.optionB = optionB;
		this.optionC = optionC;
		this.optionD = optionD;
		this.answer = answer;	
	}
	
	/**
	 * Constructor using two fields of level and question
	 * @param level the level of this question
	 * @param question the question text of this question
	 */
	public Question(int level, String question) {
		this.setLevel(level);
		this.question = question;
	}

	/**
	 * @return question text
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return answer
	 */
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getOptionA() {
		return optionA;
	}

	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}

	public String getOptionB() {
		return optionB;
	}

	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}

	public String getOptionC() {
		return optionC;
	}

	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}

	public String getOptionD() {
		return optionD;
	}

	public void setOptionD(String optionD) {
		this.optionD = optionD;
	}

	public void setOptions(String optionA, String optionB, String optionC, String optionD) {	
		this.optionA = optionA;
		this.optionB = optionB;
		this.optionC = optionC;
		this.optionD = optionD;
	}

	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public String toString() {
//		return "Question [question=" + question + ", answer=" + answer + ", optionA=" + optionA + ", optionB=" + optionB
//				+ ", optionC=" + optionC + ", optionD=" + optionD + ", level=" + level + "]";
		return " Level " + level + "\n " + question + "\n " + optionA + "\n " + optionB + "\n " + optionC + "\n " + optionD
				+ "\n " + answer + "\n\n";
	}
	
	/**
	 * @return the connection to Question Database.
	 */
	public static Connection getQuestionDBConnection() {
		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:question.db");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} 
		return connection;
	}
	
	/**
	 * insert the question object who calls this method into Question Database
	 */
	public void insertToQuestionDB() {
		Connection connection = null;
		try {
			connection = getQuestionDBConnection();
			Statement statement = connection.createStatement();
		    statement.setQueryTimeout(30); 	    
	        statement.executeUpdate("CREATE TABLE IF NOT EXISTS question (level INTEGER, question STRING, optiona STRING, optionb STRING, optionc STRING, optiond STRING, answer STRING)");
	        statement.executeUpdate("INSERT INTO question VALUES('" + level + "', '" + question + "', '" + optionA + "', '" + optionB + "', '" + optionC + "', '" + optionD + "', '" + answer + "')");  	
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @return a hash map of question_level as key and array_list_question as values. Get all questions from Database
	 */
	public static HashMap<Integer, ArrayList<Question>> getAllQuestionsFromDB() {
		HashMap<Integer, ArrayList<Question>> hm = new HashMap<>();
		Connection connection = null;
		try {
			connection = Question.getQuestionDBConnection();
			Statement statement = connection.createStatement();
		    statement.setQueryTimeout(30);     
	        statement.executeUpdate("CREATE TABLE IF NOT EXISTS question (level INTEGER, question STRING, optiona STRING, optionb STRING, optionc STRING, optiond STRING, answer STRING)");
	        ResultSet resultSet1 = statement.executeQuery("SELECT * FROM question");	
	        while (resultSet1.next()) {
	        	hm.put(resultSet1.getInt("level"), new ArrayList<Question>());       	
	        }
	        ResultSet resultSet2 = statement.executeQuery("SELECT * FROM question");
	        while (resultSet2.next()) {
	        	hm.get(resultSet2.getInt("level")).add(new Question(resultSet2.getInt("level"), resultSet2.getString("question"), resultSet2.getString("optiona"), resultSet2.getString("optionb"), resultSet2.getString("optionc"), resultSet2.getString("optiond"), resultSet2.getString("answer")));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return hm;
	}
}
