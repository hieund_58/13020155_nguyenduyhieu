import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author KiLL
 *
 */
public class ViewDatabaseFrame {

	private JFrame frmDatabase;
	private User currentUser;

	/**
	 * Create the application.
	 */
	/**
	 * @wbp.parser.constructor
	 */
	public ViewDatabaseFrame() {
		initialize();
	}
	
	/**
	 * @param currentUser the current user who views this frame
	 */
	public ViewDatabaseFrame(User currentUser) {
		this.currentUser = new User(currentUser);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDatabase = new JFrame();
		frmDatabase.setTitle("Database");
		frmDatabase.setBounds(100, 100, 712, 540);
		frmDatabase.setLocationRelativeTo(null);
		frmDatabase.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDatabase.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(44, 30, 608, 360);
		frmDatabase.getContentPane().add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		textArea.setFont(new Font("Lucida Bright", Font.BOLD, 16));
		textArea.setOpaque(false);
		textArea.setBackground(new Color(0,0,0,0));
		scrollPane.setViewportView(textArea);
		scrollPane.getViewport().setOpaque(false);
		scrollPane.setOpaque(false);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmDatabase.dispose();
				OpeningFrame openingFrame = new OpeningFrame(currentUser);
				openingFrame.start();
			}
		});
		btnBack.setFont(new Font("Century Schoolbook", Font.BOLD, 14));
		btnBack.setBounds(477, 407, 130, 69);
		frmDatabase.getContentPane().add(btnBack);
		
		JButton btnQuestions = new JButton("Questions");
		btnQuestions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText(null);
				for (int i = 1; i <= 15; i++) {
					for (Question q : Question.getAllQuestionsFromDB().get(i)) {
						textArea.append(q.toString());
					}
				}
				textArea.setCaretPosition(0);
			}
		});
		btnQuestions.setFont(new Font("Century Schoolbook", Font.BOLD, 14));
		btnQuestions.setBounds(96, 407, 130, 69);
		frmDatabase.getContentPane().add(btnQuestions);
		
		JButton btnUsers = new JButton("Users");
		btnUsers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText(null);
				for (User u : User.getAllUsersFromDB()) {
					textArea.append(u.toString());
				}
				textArea.setCaretPosition(0);
			}
		});
		btnUsers.setFont(new Font("Century Schoolbook", Font.BOLD, 14));
		btnUsers.setBounds(282, 407, 130, 69);
		frmDatabase.getContentPane().add(btnUsers);
		
//		JLabel backgroundImage = new JLabel("");
//		backgroundImage.setIcon(new ImageIcon("C:\\Users\\KiLL\\Desktop\\CC_blue_sky.jpg"));
//		backgroundImage.setBounds(0, -51, 696, 573);
//		frame.getContentPane().add(backgroundImage);
	}
	
	/**
	 * Set this frame visible
	 */
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmDatabase.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
