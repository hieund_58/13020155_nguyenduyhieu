## How to run the program:
- Set temporary classpath: `C:\>set classpath=...\Big Assignment\lib\sqlite-jdbc-3.19.3.jar;.;`
- Compile QuizGameLauncher.java: `...\Big Assignment\src\javac QuizGameLauncher.java`
- Run the application: `...\Big Assignment\src\java QuizGameLauncher`
## The program's structure:
- **3 Object classes**
    * Question.java: *a question has its text, 4 option (A/B/C/D), an answer and the level (from 1-15, the later the harder).*
    * QuestionGenerator.java: *to generate random set of questions each game. Questions are stored in certain database.*
    * User.java: *an user has 4 attributes: name, password, best score, and type (admin/player). Best score is updated when finishing each game. Users are stored in certain database.*
- **1 Main class**
    * QuizGameLauncher.java
- **7 GUI classes**
    * LoginFrame.java
    * OpeningFrame.java
    * InGameFrame.java
    * EndGameFrame.java
    * HighScoreFrame.java
    * AddQuestionFrame.java
    * ViewDatabaseFrame.java
## Used libraries:
- *javax.swing*: to create GUI
- *java.sql*: to manage database
## How the program works:
When QuizGameLauncher runs, it will create user database (`user.db`) and question database (`question.db`). 2 default users are created, one with
admin role and one as player role. Question samples are added to `question.db`, only at the first run. The frame 'Login' appears shortly and user
interacts with it. Clicking certain buttons will change the frame: closing the current one and invoke the new. Each frame (except LoginFrame) has a private User as an
attribute to know which current user is viewing it, and we can move back and forth between them. :+1: